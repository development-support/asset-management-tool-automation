VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} ProgressInfo 
   Caption         =   "Execute Status"
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   360
   ClientWidth     =   4710
   OleObjectBlob   =   "ProgressInfo.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "ProgressInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub PercentOfProgress_Click()

End Sub

Private Sub UserForm_Initialize()
  With ProgressBar1
    .Min = 0
    If (ProgressInfoMaxCounter = 0) Then
      .Max = 100
    Else
      .Max = ProgressInfoMaxCounter
    End If
    .Value = 0
  End With

  PercentOfProgress.Caption = ""
End Sub
