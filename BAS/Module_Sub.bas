Attribute VB_Name = "Module_Sub"
Option Explicit

' シートチェック用Function
' [引数]
'   rowLast As Long：「Update_List」シートの「Update target item」列の最終行
'
' [戻り値]
'   Boolean：True  (指定シートは全て存在する)
'          ：False (指定シートの一部が存在しない)
Public Function CheckSheetExist(ByVal rowLast As Long) As Boolean
  Dim rowCur As Long
  Dim strSheetName As String

  ' 最終行まで1行毎に処理
  For rowCur = ROW_START To rowLast
    ' 「Sheet name」の記載がある場合、値を取得して存在をチェック
    If Cells(rowCur, ColList.colSheetName) <> "" Then
      strSheetName = Cells(rowCur, ColList.colSheetName)
      Debug.Print "(CheckSheetExist):SheetName = " & strSheetName
    Else
      GoTo SHEET_CHECK_SKIP
    End If

    ' 「Sheet name」に記載のシートが存在しない場合、強制終了
    If isSheetExist(strSheetName) = False Then
      MsgBox strSheetName & "シートがありません", vbExclamation
      CheckSheetExist = False
      Exit Function
    End If

SHEET_CHECK_SKIP:
  Next rowCur

  CheckSheetExist = True

End Function

' 指定シートが存在するかチェックするFunction
' [引数]
'   strSheetName As String：シート名
'
' [戻り値]
'   Boolean：True  (指定シートが存在する)
'          ：False (指定シートが存在しない)
Public Function isSheetExist(ByVal strSheetName As String) As Boolean
  Dim ws As Worksheet
  Dim FindFlg As Boolean

  FindFlg = False

  For Each ws In Worksheets
    If ws.Name = strSheetName Then
      FindFlg = True
      Exit For
    End If
  Next ws

  If FindFlg = False Then
    isSheetExist = False
    Exit Function
  End If

  isSheetExist = True

End Function

' ログファイル名一覧取得用Function
' [引数]
'   strPath As String：ログファイル格納パス
'   strFileName() As String：ログファイル名格納用の動的配列
'
' [戻り値]
'   Boolean：True  (取得成功)
'          ：False (取得失敗[ログファイルがひとつも無い])
Public Function GetLogFileList(ByVal strPath As String, _
                               ByRef strFileName() As String) As Boolean
  Dim buf As String
  Dim Cnt As Long
  Dim Target

  Cnt = 0
  buf = Dir(strPath & "\" & LOG_FILE_EXT)
  ' 動的配列の初期化
  ReDim strFileName(Cnt)

  Do While buf <> ""
    Cnt = Cnt + 1
    ReDim Preserve strFileName(Cnt)

    strFileName(Cnt) = buf
    Debug.Print "(GetLogFileList):strFileName(" & Cnt & ") = " & strFileName(Cnt)

    buf = Dir()
  Loop

  If (Cnt = 0) Then
    MsgBox strPath & "配下にログファイル(" & LOG_FILE_EXT & _
                     ")がありません", vbExclamation
    GetLogFileList = False
    Exit Function
  End If

  GetLogFileList = True

End Function

' 更新履歴シート作成用Function
' [引数]
'   strHistorySheet As String：作成する更新履歴シート名
'
' [戻り値]
'   無し
Public Function CreateHistorySheet(ByVal strHistorySheet As String)
  Dim ws As Worksheet
  Dim wsNew As Worksheet
  Dim i As Long, j As Long
  Dim swap As Long
  Dim HistoryCnt As Long
  Dim HistoryName() As String

  HistoryCnt = 0
  ReDim HistoryName(HistoryCnt)

  ' 更新履歴シート(History_xxxx)がいくつあるか調べる
  For Each ws In Worksheets
    If InStr(ws.Name, HISTORY_SHEET) > 0 Then
      HistoryCnt = HistoryCnt + 1
      ReDim Preserve HistoryName(HistoryCnt)

      HistoryName(HistoryCnt) = ws.Name
    End If
  Next ws

  ' 更新履歴シートが最大世代以上ある場合、一番古いシートを削除する
  If (HistoryCnt = HISTORY_GEN_MAX) Then
    '配列の要素をソートする
    For i = 1 To HistoryCnt
      For j = HistoryCnt To i Step -1
        If HistoryName(i) > HistoryName(j) Then
          swap = HistoryName(i)
          HistoryName(i) = HistoryName(j)
          HistoryName(j) = swap
        End If
      Next j
    Next i

    ' 一番古いシート(値が最小)を削除する
    Application.DisplayAlerts = False
    Worksheets(HistoryName(1)).Delete
    Application.DisplayAlerts = True
  End If

  ' 新規シート作成
  Set wsNew = Worksheets.Add(After:=Worksheets(Worksheets.Count))
  wsNew.Name = strHistorySheet

  ' 見出しを設定する
  Cells(1, eHistoryHead.UpdateSheet) = "Update Sheet"
  Cells(1, eHistoryHead.ItemName) = "Item Name"
  Cells(1, eHistoryHead.SerialNum) = "Serial Number"
  Cells(1, eHistoryHead.IssueNum) = "Issue Number"
  Cells(1, eHistoryHead.UpdateItem) = "Update Item"
  Cells(1, eHistoryHead.PreValue) = "Previous Value"
  Cells(1, eHistoryHead.UpdateValue) = "Updated Value"
  Cells(1, eHistoryHead.UpdateStatus) = "Update Status"
  Cells(1, eHistoryHead.LogFileName) = "Logfile Name"

  ' 各見出しセルのフォント、背景色を設定する
  For i = eHistoryHead.UpdateSheet To eHistoryHead.LogFileName
    ' フォント設定(太字)
    Range(Cells(1, i).Address).Font.Bold = True
    ' フォント種別を指定
    Range(Cells(1, i).Address).Font.Name = "Meiryo UI"
    ' フォントサイズを指定
    Range(Cells(1, i).Address).Font.Size = 10
    ' セルの背景色を設定
    Range(Cells(1, i).Address).Interior.Color = RGB(50, 200, 0)
    ' セルの幅を自動調整
    Range(Cells(1, i).Address).Columns.AutoFit
    ' 罫線を引く
    Range(Cells(1, i).Address).Borders.LineStyle = True
    ' 下側の罫線の種類 =　二重線
    Range(Cells(1, i).Address).Borders(xlEdgeBottom).LineStyle = xlDouble
  Next i

  ' 更新履歴記入シートの書込み行を設定
  rowCurHistory = 2

  ' 元のシートへ切替
  Worksheets(UpdateListSheet).Activate

End Function

' 更新履歴シートの列全体幅自動調整用Function
' [引数]
'   strHistorySheet As String：作成する更新履歴シート名
'
' [戻り値]
'   無し
Public Function AutoFitHistorySheet(ByVal strHistorySheet As String)
  Dim i As Long

  ' 更新履歴シートへ切替
  Worksheets(strHistorySheet).Activate

  ' 各見出しセルのフォント、背景色を設定する
  For i = eHistoryHead.UpdateSheet To eHistoryHead.LogFileName
    ' 列全体の幅を自動調整
    Range(Cells(1, i).Address).EntireColumn.AutoFit
  Next i

  ' 元のシートへ切替
  Worksheets(UpdateListSheet).Activate

End Function

' ログファイル読込み用Function
' [引数]
'   strPath As String     ：ログファイル格納パス
'   strFileName As String ：ログファイル名
'   strTempSheet As String：作業用シート名
'
' [戻り値]
'   無し
Public Function ReadLogFile(ByVal strPath As String, _
                            ByVal strFileName As String, _
                            ByVal strTempSheet As String)
  Dim ws As Worksheet
  Dim wsNew As Worksheet
  Dim strOpenFile As String
  Dim buf As String
  Dim i As Long

  Debug.Print "(ReadLogFile):strPath = " & strPath & vbCrLf & _
              "              strFileName = " & strFileName & _
                           " strTempSheet = " & strTempSheet

  ' 指定シート名と同名シートがある場合、シートを削除する
  For Each ws In Worksheets
    If ws.Name = strTempSheet Then
      Application.DisplayAlerts = False
      Worksheets(strTempSheet).Delete
      Application.DisplayAlerts = True
      Exit For
    End If
  Next ws

  ' 新規シート作成
  Set wsNew = Worksheets.Add()
  wsNew.Name = strTempSheet

  strOpenFile = strPath & "\" & strFileName
  Open strOpenFile For Input As #1

    i = 1
    Do Until EOF(1)
      Line Input #1, buf
      ' １文字目が"="文字の場合、先頭にカンマを付与する
      If (InStr(buf, "=") = 1) Then
        buf = "'" & buf
      End If
      Cells(i, 1).Value = buf
      i = i + 1
    Loop
  
  Close #1

  ' 元のシートへ切替
  Worksheets(UpdateListSheet).Activate

End Function

' ログ出力シート削除用Function
' [引数]
'   strTempSheet As String：作業用シート名
'
' [戻り値]
'   無し
Public Function DeleteLogSheet(ByVal strTempSheet As String)
  Dim ws As Worksheet

  Debug.Print "(DeleteLogSheet):strTempSheet = " & strTempSheet

  ' 指定シート名と同名シートがある場合、シートを削除する
  For Each ws In Worksheets
    If ws.Name = strTempSheet Then
      Application.DisplayAlerts = False
      Worksheets(strTempSheet).Delete
      Application.DisplayAlerts = True
      Exit For
    End If
  Next ws

End Function

' 更新対象Unit関連情報(ログファイル名除く)の初期化用Function
' [引数]
'   UnitInfoList As tUnitInfoList：初期化対象の更新対象Unit関連情報
'
' [戻り値]
'   無し
Public Function InitUnitInfoList(ByRef UnitInfoList As tUnitInfoList)
  Dim InitList As tUnitInfoList
  Dim tmpLogFileName As String
  Dim tmpShelfConf As eShelfConf

  ' ログファイル名/Shelf構成判別結果は初期化しないので一時退避しておく
  tmpLogFileName = UnitInfoList.LogFileName
  tmpShelfConf = UnitInfoList.ShelfConf
  ' 空の同じ定義型の変数をコピーして初期化
  UnitInfoList = InitList
  ' ログファイル名/Shelf構成判別結果を戻しておく
  UnitInfoList.LogFileName = tmpLogFileName
  UnitInfoList.ShelfConf = tmpShelfConf

End Function

' UnitNameがログに存在するか検索するFunction
' [引数]
'   strUnitName As String ：「unitName (at Log)」に記載のUnit名
'   strTempSheet As String：作業用シート名
'
' [戻り値]
'   Boolean：True  (指定UnitNameがログ中に存在する)
'          ：False (指定UnitNameがログ中に存在しない)
Public Function FindUnitNameFromLog(ByVal strUnitName As String, _
                                    ByVal strTempSheet As String) As Boolean
  Dim RExp As Variant
  Dim strPattern As String
  Dim SearchRng As Range
  Dim i As Long

  Debug.Print "(FindUnitNameFromLog):strUnitName = " & strUnitName & _
              " strTempSheet = " & strTempSheet

  Set RExp = CreateObject("VBScript.RegExp")
  ' "+"文字がある場合、正規表現検索できるよう"\+"に置換する
  If (InStr(strUnitName, "+") <> 0) Then
    strUnitName = Replace(strUnitName, "+", "\+")
  End If
  strPattern = strUnitName & "$"  ' UnitNameにメタ文字(文字列の末尾)を追加

  UnitInfoList.FindNum = 0  ' 検索Hitしたセルの個数
  ' 動的配列の初期化
  ReDim UnitInfoList.RngFind(UnitInfoList.FindNum)
  ReDim UnitInfoList.RngDipSSH(UnitInfoList.FindNum)
  ReDim UnitInfoList.RngDipEnd(UnitInfoList.FindNum)
  ReDim UnitInfoList.SerialNum(UnitInfoList.FindNum)
  ReDim UnitInfoList.IssueNum(UnitInfoList.FindNum)
  ReDim UnitInfoList.ShelfNum(UnitInfoList.FindNum)
  ReDim UnitInfoList.SlotNum(UnitInfoList.FindNum)
  ReDim UnitInfoList.VendorName(UnitInfoList.FindNum)
  ReDim UnitInfoList.FcNum(UnitInfoList.FindNum)
  ReDim UnitInfoList.Clei(UnitInfoList.FindNum)
  ReDim UnitInfoList.Dom(UnitInfoList.FindNum)

  ' ログ一時出力シートに切替
  Worksheets(strTempSheet).Activate

  With RExp
    .Pattern = strPattern  ' 検索パターンを設定
    .Global = True         ' 文字列全体を検索

    For Each SearchRng In ActiveSheet.UsedRange
      If .Test(SearchRng.Formula) Then
        ' 検索Hitした個数をインクリメント
        UnitInfoList.FindNum = UnitInfoList.FindNum + 1
        ' 動的配列を再定義
        ReDim Preserve UnitInfoList.RngFind(UnitInfoList.FindNum)
        ReDim UnitInfoList.RngDipSSH(UnitInfoList.FindNum)
        ReDim UnitInfoList.RngDipEnd(UnitInfoList.FindNum)
        ReDim UnitInfoList.SerialNum(UnitInfoList.FindNum)
        ReDim UnitInfoList.IssueNum(UnitInfoList.FindNum)
        ReDim UnitInfoList.ShelfNum(UnitInfoList.FindNum)
        ReDim UnitInfoList.SlotNum(UnitInfoList.FindNum)
        ReDim UnitInfoList.VendorName(UnitInfoList.FindNum)
        ReDim UnitInfoList.FcNum(UnitInfoList.FindNum)
        ReDim UnitInfoList.Clei(UnitInfoList.FindNum)
        ReDim UnitInfoList.Dom(UnitInfoList.FindNum)
        ' 対応する配列に検索HitしたRangeオブジェクトを格納
        Set UnitInfoList.RngFind(UnitInfoList.FindNum) = SearchRng
      End If
    Next SearchRng
  End With

  Set RExp = Nothing

  If UnitInfoList.FindNum = 0 Then
    ' 元のシートに切替
    Worksheets(UpdateListSheet).Activate
    FindUnitNameFromLog = False

    Exit Function
  End If

  Debug.Print "(FindUnitNameFromLog):UnitInfoList.UnitName = " & UnitInfoList.UnitName & _
              " UnitInfoList.FindNum = " & UnitInfoList.FindNum

  For i = 1 To UnitInfoList.FindNum
    Debug.Print "               UnitInfoList.RngFind(" & i & ").Value = " & _
                UnitInfoList.RngFind(i).Value & vbCrLf
  Next i

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate
  FindUnitNameFromLog = True

End Function

' Serial Number/Issue Number等をログから検索するFunction
' [引数]
'   strUnitName As String ：「unitName (at Log)」に記載のUnit名
'   strSearchKey As String：検索キー(文字列)
'   strDirection As String：検索方向(SEARCH_NEXT/SEARCH_PREV)
'   DArray() As String    ：検索キーにより取得した要素を格納する動的配列
'   strTempSheet As String：作業用シート名
'
' [戻り値]
'   Boolean：True  (ログから指定要素の取得に成功)
'          ：False (ログから指定要素の取得に失敗)
Public Function FindUnitInfoFromLog(ByVal strUnitName As String, _
                                    ByVal strSearchKey As String, _
                                    ByVal strDirection As String, _
                                    ByRef DArray() As String, _
                                    ByVal strTempSheet As String) As Boolean
  Dim FoundCell As Range
  Dim i As Long
  Dim MidPos As Long

  Debug.Print "(FindUnitInfoFromLog):strUnitName = " & strUnitName & _
              " strSearchKey = " & strSearchKey & _
              " strDirection = " & strDirection & _
              " strTempSheet = " & strTempSheet

  ' ログ一時出力シートに切替
  Worksheets(strTempSheet).Activate

  For i = 1 To UnitInfoList.FindNum
    Select Case (strDirection)
      Case SEARCH_NEXT
        Set FoundCell = Columns(1).Find(What:=strSearchKey, _
                                        After:=UnitInfoList.RngFind(i).Cells, _
                                        SearchDirection:=xlNext, _
                                        LookAt:=xlPart)
      Case SEARCH_PREV
        Set FoundCell = Columns(1).Find(What:=strSearchKey, _
                                        After:=UnitInfoList.RngFind(i).Cells, _
                                        SearchDirection:=xlPrevious, _
                                        LookAt:=xlPart)
      Case Else
        Set FoundCell = Columns(1).Find(What:=strSearchKey, _
                                        After:=UnitInfoList.RngFind(i).Cells, _
                                        SearchDirection:=xlNext, _
                                        LookAt:=xlPart)
    End Select

    If FoundCell Is Nothing Then
      ' 元のシートに切替
      Worksheets(UpdateListSheet).Activate
      FindUnitInfoFromLog = False
      Exit Function
    Else
      DArray(i) = FoundCell.Value
      ' Debug.Print "(FindUnitInfoFromLog):FoundCell.Value = " & FoundCell.Value

      ' strSearchKeyより右側の文字列を抜き出す
      MidPos = InStr(DArray(i), strSearchKey) + Len(strSearchKey)
      DArray(i) = Mid(DArray(i), MidPos)
      ' 文字列から空白を削除
      DArray(i) = Replace(DArray(i), " ", "")
      Debug.Print "(FindUnitInfoFromLog):DArray(" & i & ") = " & DArray(i)
    End If
  Next i

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate
  FindUnitInfoFromLog = True

End Function

' Shelf/Slot Numberをログから検索するFunction
' [引数]
'   strUnitName As String  ：「unitName (at Log)」に記載のUnit名
'   strSearchKey As String ：検索キー(文字列)
'   DArrayShelf() As String：検索キーにより取得したShelf番号を格納する動的配列
'   DArraySlot() As String ：検索キーにより取得したSlot番号を格納する動的配列
'   strTempSheet As String ：作業用シート名
'
' [戻り値]
'   Boolean：True  (ログからSlot番号の取得に成功)
'          ：False (ログからSlot番号の取得に失敗)
Public Function FindShelfSlotInfoFromLog(ByVal strUnitName As String, _
                                         ByVal strSearchKey As String, _
                                         ByRef DArrayShelf() As String, _
                                         ByRef DArraySlot() As String, _
                                         ByVal strTempSheet As String) As Boolean
  Dim FoundCell As Range
  Dim i As Long
  Dim MidPos As Long
  Dim strLen As Long

  Debug.Print "(FindShelfSlotInfoFromLog):strUnitName = " & strUnitName & _
              " strSearchKey = " & strSearchKey & _
              " strTempSheet = " & strTempSheet

  ' ログ一時出力シートに切替
  Worksheets(strTempSheet).Activate

  For i = 1 To UnitInfoList.FindNum
    Set FoundCell = Columns(1).Find(What:=strSearchKey, _
                                    After:=UnitInfoList.RngFind(i).Cells, _
                                    LookAt:=xlPart, _
                                    SearchDirection:=xlPrevious)
    If FoundCell Is Nothing Then
      ' 元のシートに切替
      Worksheets(UpdateListSheet).Activate
      FindShelfSlotInfoFromLog = False
      Exit Function
    Else
      ' Debug.Print "(FindShelfSlotInfoFromLog):FoundCell.Value = " & FoundCell.Value
      DArrayShelf(i) = FoundCell.Value
      DArraySlot(i) = FoundCell.Value

      ' [例] inventoryName  "Shelf: 1 slot: 0 subslot: 0 port: 0"
      ' **** Shelf番号取得処理 ****
      ' "Shelf:"より右側の文字列を抜き出す
      MidPos = InStr(DArrayShelf(i), LOG_SHELF) + Len(LOG_SHELF)
      DArrayShelf(i) = Mid(DArrayShelf(i), MidPos)

      ' 文字列先頭から"slot:"の直前までの文字列を抜き出す
      strLen = InStr(DArrayShelf(i), LOG_SLOT) - 1
      DArrayShelf(i) = Left(DArrayShelf(i), strLen)

      ' 文字列から空白を削除
      DArrayShelf(i) = Replace(DArrayShelf(i), " ", "")

      ' **** Slot番号取得処理 ****
      ' "slot:"より右側の文字列を抜き出す
      MidPos = InStr(DArraySlot(i), LOG_SLOT) + Len(LOG_SLOT)
      DArraySlot(i) = Mid(DArraySlot(i), MidPos)

      ' 文字列先頭から"subslot:"の直前までの文字列を抜き出す
      strLen = InStr(DArraySlot(i), LOG_SUB_SLOT) - 1
      DArraySlot(i) = Left(DArraySlot(i), strLen)

      ' 文字列から空白を削除
      DArraySlot(i) = Replace(DArraySlot(i), " ", "")
      Debug.Print "(FindSlotInfoFromLog):DArrayShelf(" & i & ") = " & DArrayShelf(i) & _
                  " DArraySlot(" & i & ") = " & DArraySlot(i)
    End If
  Next i

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate
  FindShelfSlotInfoFromLog = True

End Function

' Shelf構成をログから判断するFunction
' [引数]
'   strShelfConf As eShelfConf：Shelf構成判別結果
'   strTempSheet As String    ：作業用シート名
'
' [戻り値]
'   なし
Public Function JudgeShelfConf(ByRef tShelfConf As eShelfConf, _
                               ByVal strTempSheet As String)
  Dim RExp As Variant
  Dim strPatternA As String
  Dim strPatternB As String
  Dim SearchRng As Range
  Dim NumC200 As Long
  Dim NumC220 As Long

  Select Case tShelfConf
    ' 初回のみShelf構成を判定
    Case INIT_STATUS
      ' ログ一時出力シートに切替
      Worksheets(strTempSheet).Activate

      ' unitName = BDC2-C200, BDC2-C220がログ上にあるか検索
      Set RExp = CreateObject("VBScript.RegExp")
      strPatternA = UNIT_C200 & "$"  ' UnitNameにメタ文字(文字列の末尾)を追加
      strPatternB = UNIT_C220 & "$"  ' UnitNameにメタ文字(文字列の末尾)を追加

      With RExp
        .Pattern = strPatternA  ' 検索パターンを設定
        .Global = True          ' 文字列全体を検索

        For Each SearchRng In ActiveSheet.UsedRange
          If .Test(SearchRng.Formula) Then
            NumC200 = NumC200 + 1
          End If
        Next SearchRng
      End With

      With RExp
        .Pattern = strPatternB  ' 検索パターンを設定
        .Global = True          ' 文字列全体を検索

        For Each SearchRng In ActiveSheet.UsedRange
          If .Test(SearchRng.Formula) Then
            NumC220 = NumC220 + 1
          End If
        Next SearchRng
      End With

      Set RExp = Nothing

      If (NumC200 > 0) Or (NumC220 > 0) Then
        tShelfConf = AGGREGATOR_MODE
      Else
        tShelfConf = STANDALONE_MODE
      End If

      Debug.Print "(JudgeShelfConf):tShelfConf = " & tShelfConf

      ' 元のシートに切替
      Worksheets(UpdateListSheet).Activate

    Case STANDALONE_MODE, AGGREGATOR_MODE
      ' NOP
  End Select

End Function

' Shelf構成がAggregatorの場合、ログ上で該当Unitの情報を取得できるか
' をチェックするFunction
' (ログ上に以下の文字列があるかを検索して、該当セルを取得)
'   " ssh -p 6022 fujitsu@127.1.1." + Shelf番号
'   "Connection to 127.1.1." + Shelf番号
'
' [引数]
'   UnitIndex As Long     ：検索で見つかったUnitのindex番号
'   strTempSheet As String：作業用シート名
'
' [戻り値]
'   Boolean：True  (チェックOK)
'          ：False (チェックNG)
Public Function CheckDipSSHFromLog(ByVal UnitIndex As Long, _
                                   ByVal strTempSheet As String) As Boolean
  Dim FoundCell As Range
  Dim i As Long
  Dim strDipSSH As String
  Dim strDipClose As String
  Dim MidPos As Long

  Debug.Print "(CheckDipSSHFromLog):UnitIndex = " & UnitIndex & _
              " strTempSheet = " & strTempSheet

  ' ログ一時出力シートに切替
  Worksheets(strTempSheet).Activate

  strDipSSH = DIP_SSH & UnitInfoList.ShelfNum(UnitIndex)
  strDipClose = DIP_END & UnitInfoList.ShelfNum(UnitIndex)

  ' Dip切替開始セルを検索
  Set FoundCell = Columns(1).Find(What:=strDipSSH, LookAt:=xlPart)
  If FoundCell Is Nothing Then
    ' 元のシートに切替
    Worksheets(UpdateListSheet).Activate
    CheckDipSSHFromLog = False
    Exit Function
  End If

  Set UnitInfoList.RngDipSSH(UnitIndex) = FoundCell
  Debug.Print "(CheckDipSSHFromLog):UnitInfoList.RngDipSSH(" & UnitIndex & ") = " & _
              FoundCell.Value

  ' Dip切替終了セルを検索
  Set FoundCell = Columns(1).Find(What:=strDipClose, LookAt:=xlPart)
  If FoundCell Is Nothing Then
    ' 元のシートに切替
    Worksheets(UpdateListSheet).Activate
    CheckDipSSHFromLog = False
    Exit Function
  End If

  Set UnitInfoList.RngDipEnd(UnitIndex) = FoundCell
  Debug.Print "(CheckDipSSHFromLog):UnitInfoList.RngDipEnd(" & UnitIndex & ") = " & _
              FoundCell.Value

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate
  CheckDipSSHFromLog = True

End Function

' 指定シートにおいて、指定文字列(見出し)のセル列を特定するFunction
' [引数]
'   strSheetName As String：各Unitのシート名
'   strHead As String     ：見出し文字列
'   colNum As Long        ：列番号を格納する変数
'
' [戻り値]
'   Boolean：True  (指定文字列の見出しセル列取得に成功)
'          ：False (指定文字列の見出しセル列取得に失敗)
Public Function FindHeadColFromList(ByVal strSheetName As String, _
                                    ByVal strHead As String, _
                                    ByRef colNum As Long) As Boolean
  Dim FoundCell As Range
  Dim i As Long

  ' 各Unitのシートに切替
  Worksheets(strSheetName).Activate

  Set FoundCell = Cells.Find(What:=strHead, LookAt:=xlWhole)
  If FoundCell Is Nothing Then
    colNum = 0
    ' 元のシートに切替
    Worksheets(UpdateListSheet).Activate
    Debug.Print "(FindHeadColFromList):strSheetName = " & strSheetName & _
                " strHead = " & strHead & " colNum = " & colNum
    FindHeadColFromList = False
    Exit Function
  End If

  colNum = FoundCell.Column
  Debug.Print "(FindHeadColFromList):strSheetName = " & strSheetName & _
              " strHead = " & strHead & " colNum = " & colNum

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate
  FindHeadColFromList = True

End Function

' 指定シートにおいて、指定Itemの行数をItem Name/Serial Number/Issue Numberを
' 基に特定するFunction
' [引数]
'   strSheetName As String：各Unitのシート名
'   Mode As String        ：物品判定モード
'   ItemNum As Long       ：ログ検索で見つかったUnitのIndex番号
'
' [戻り値]
'   eFindTargetRowFromList：ITEM_FOUND_SUCCESS (指定Unitの行数取得に成功)
'                         ：ITEM_NOT_FOUND (指定Unitが管理簿に無い)
'                         ：MULTI_ITEM_FOUND (指定Unitが複数見つかった)
Public Function FindTargetRowFromList(ByVal strSheetName As String, _
                                      ByVal strMode As String, _
                                      ByVal ItemNum As Long) As eFindTargetRowFromList
  Dim FoundCell As Range
  Dim RngTarget() As Range
  Dim FoundNum As Long
  Dim ValidItemNum As Long
  Dim ValidIssueNum As Long
  Dim i As Long

  Debug.Print "(FindTargetRowFromList):strSheetName = " & strSheetName & _
              " strMode = " & strMode & " ItemNum = " & ItemNum

  ' 各Unitのシートに切替
  Worksheets(strSheetName).Activate

  Select Case (strMode)
    ' Serial Numberのみでの判定
    Case MODE_SN_ONLY
      FoundNum = 0
      ' 動的配列の初期化
      ReDim RngTarget(FoundNum)

      Set FoundCell = Cells.Find(What:=UnitInfoList.SerialNum(ItemNum), LookAt:=xlWhole)
      ' 対象のSerial Numberが見つからなかった場合、エラー終了
      If FoundCell Is Nothing Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        FindTargetRowFromList = ITEM_NOT_FOUND
        Exit Function
      Else
        FoundNum = FoundNum + 1
        ' 動的配列の再定義
        ReDim RngTarget(FoundNum)
        Set RngTarget(FoundNum) = FoundCell
      End If

      Do
        Set FoundCell = Cells.FindNext(FoundCell)
        ' 一番最初に見つかったセルと同じなら抜ける
        If FoundCell.Address = RngTarget(1).Address Then
          Exit Do
        Else
          FoundNum = FoundNum + 1
          ' 動的配列の再定義
          ReDim Preserve RngTarget(FoundNum)
          Set RngTarget(FoundNum) = FoundCell
        End If
      Loop

      Debug.Print "(FindTargetRowFromList):FoundNum = " & FoundNum

      ' 同一Serial Numberの物品が複数ある場合、エラー終了
      If (FoundNum > 1) Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        FindTargetRowFromList = MULTI_ITEM_FOUND
        Exit Function
      End If

      ' 対象行数を取得
      UnitInfoList.rowTargetItem = FoundCell.Row

    ' Serial NumberとItem Nameによる判定
    Case MODE_SN_NAME
      FoundNum = 0
      ' 動的配列の初期化
      ReDim RngTarget(FoundNum)

      Set FoundCell = Cells.Find(What:=UnitInfoList.SerialNum(ItemNum), LookAt:=xlWhole)
      ' 対象のSerial Numberが見つからなかった場合、エラー終了
      If FoundCell Is Nothing Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        FindTargetRowFromList = ITEM_NOT_FOUND
        Exit Function
      ' 対象のSerial Numberが見つかった場合、最初に見つかったセルを保存
      Else
        FoundNum = FoundNum + 1
        ' 動的配列の再定義
        ReDim RngTarget(FoundNum)
        Set RngTarget(FoundNum) = FoundCell
      End If

      Do
        Set FoundCell = Cells.FindNext(FoundCell)
        ' 一番最初に見つかったセルと同じなら抜ける
        If FoundCell.Address = RngTarget(1).Address Then
          Exit Do
        Else
          FoundNum = FoundNum + 1
          ' 動的配列の再定義
          ReDim Preserve RngTarget(FoundNum)
          Set RngTarget(FoundNum) = FoundCell
        End If
      Loop

      Debug.Print "(FindTargetRowFromList):FoundNum = " & FoundNum
      ValidItemNum = 0

      For i = 1 To FoundNum
        ' 同行数のItem Nameをチェックし、Update_Listシートに記載のものと合っていればカウント
        If (Cells(RngTarget(i).Row, UnitInfoList.colItemName) = UnitInfoList.ItemName) Then
          ValidItemNum = ValidItemNum + 1
          ' 対象行数を取得
          UnitInfoList.rowTargetItem = RngTarget(i).Row
        End If
      Next i

      Debug.Print "(FindTargetRowFromList):ValidItemNum = " & ValidItemNum

      ' Serial Numberは同じで、Itemn Nameが異なる物品の場合、エラー終了
      If (ValidItemNum = 0) Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        FindTargetRowFromList = ITEM_NOT_FOUND
        Exit Function
      ' Serial NumberとItem Nameが同一の物品が複数ある場合、エラー終了
      ElseIf (ValidItemNum > 1) Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        FindTargetRowFromList = MULTI_ITEM_FOUND
        Exit Function
      End If

    ' Serial NumberとItem Name、Issue Numberによる判定
    Case MODE_SN_NAME_ISS
      FoundNum = 0
      ' 動的配列の初期化
      ReDim RngTarget(FoundNum)

      Set FoundCell = Cells.Find(What:=UnitInfoList.SerialNum(ItemNum), LookAt:=xlWhole)
      ' 対象のSerial Numberが見つからなかった場合、エラー終了
      If FoundCell Is Nothing Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        FindTargetRowFromList = ITEM_NOT_FOUND
        Exit Function
      ' 対象のSerial Numberが見つかった場合、最初に見つかったセルを保存
      Else
        FoundNum = FoundNum + 1
        ' 動的配列の再定義
        ReDim RngTarget(FoundNum)
        Set RngTarget(FoundNum) = FoundCell
      End If

      Do
        Set FoundCell = Cells.FindNext(FoundCell)
        ' 一番最初に見つかったセルと同じなら抜ける
        If FoundCell.Address = RngTarget(1).Address Then
          Exit Do
        Else
          FoundNum = FoundNum + 1
          ' 動的配列の再定義
          ReDim Preserve RngTarget(FoundNum)
          Set RngTarget(FoundNum) = FoundCell
        End If
      Loop

      Debug.Print "(FindTargetRowFromList):FoundNum = " & FoundNum
      ValidItemNum = 0

      For i = 1 To FoundNum
        ' 同行数のItem Nameをチェックし、Update_Listシートに記載のものと合っていればカウント
        If (Cells(RngTarget(i).Row, UnitInfoList.colItemName) = UnitInfoList.ItemName) Then
          ValidItemNum = ValidItemNum + 1
        End If
      Next i

      Debug.Print "(FindTargetRowFromList):ValidItemNum = " & ValidItemNum

      ' Serial Numberは同じで、Itemn Nameが異なる物品の場合、エラー終了
      If (ValidItemNum = 0) Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        FindTargetRowFromList = ITEM_NOT_FOUND
        Exit Function
      End If

      ' Serial NumberとUnit Nameが同一の物品がある場合、Issue Numberを確認する
      If (ValidItemNum >= 1) Then
        ValidIssueNum = 0

        For i = 1 To FoundNum
          ' 同行数のIssue Numberをチェックし、ログ上のものと合っていればカウント
          If (Cells(RngTarget(i).Row, UnitInfoList.colIss) = _
              UnitInfoList.IssueNum(ItemNum)) Then
            ValidIssueNum = ValidIssueNum + 1
            ' 対象行数を取得
            UnitInfoList.rowTargetItem = RngTarget(i).Row
          End If
        Next i
      End If

      Debug.Print "(FindTargetRowFromList):ValidIssueNum = " & ValidIssueNum

      ' Serial Number, Item Nameは同じで、Issue Numberが異なる物品の場合、エラー終了
      If (ValidIssueNum = 0) Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        FindTargetRowFromList = ITEM_NOT_FOUND
        Exit Function
      ' Serial Number, Item Name, Issue Numberが同じ物品が複数ある場合、エラー終了
      ElseIf (ValidIssueNum > 1) Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        FindTargetRowFromList = MULTI_ITEM_FOUND
        Exit Function
      End If
  End Select

  Debug.Print "(FindTargetRowFromList):UnitInfoList.rowTargetItem = " & UnitInfoList.rowTargetItem
  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate
  FindTargetRowFromList = ITEM_FOUND_SUCCESS

End Function

' 更新履歴シートへの履歴書込み用Function
' [引数]
'   index As Long            ：Unit検索で見つかったインデックス番号
'
' [戻り値]
'   無し
Public Function WriteHistory(ByVal index As Long)
  Dim ws As Worksheet
  Dim rowCur As Long
  Dim i As Long

  Set ws = Worksheets(HistorySheet)
  rowCur = rowCurHistory

  ' 更新履歴シートに切替
  ws.Activate

  ' **** セルの各種設定。幅はまとめて自動調整する。****
  For i = eHistoryHead.UpdateSheet To eHistoryHead.LogFileName
    ' 罫線を引く
    Range(ws.Cells(rowCur, i).Address).Borders.LineStyle = True
    ' 表示形式を「文字列」に設定
    Range(ws.Cells(rowCur, i).Address).NumberFormatLocal = "@"
    ' フォント種別を指定
    Range(ws.Cells(rowCur, i).Address).Font.Name = "Meiryo UI"
    ' フォントサイズを指定
    Range(ws.Cells(rowCur, i).Address).Font.Size = 9
  Next i

  ' Update sheetを記入
  ws.Cells(rowCur, eHistoryHead.UpdateSheet) = UnitInfoList.SheetName
  ' Item Nameを記入
  ws.Cells(rowCur, eHistoryHead.ItemName) = UnitInfoList.ItemName
  ' Serial numberを記入
  ws.Cells(rowCur, eHistoryHead.SerialNum) = UnitInfoList.SerialNum(index)
  ' Issue numberを記入
  ws.Cells(rowCur, eHistoryHead.IssueNum) = UnitInfoList.IssueNum(index)
  ' Update Itemを記入
  ws.Cells(rowCur, eHistoryHead.UpdateItem) = UnitInfoList.UpdateItem
  ' Pre Valueを記入
  ws.Cells(rowCur, eHistoryHead.PreValue) = UnitInfoList.PreValue
  ' Update Valueを記入
  ws.Cells(rowCur, eHistoryHead.UpdateValue) = UnitInfoList.UpValue
  ' Update Statusを記入
  ws.Cells(rowCur, eHistoryHead.UpdateStatus) = UnitInfoList.HisStatus
  ' Logfile nameを記入
  ws.Cells(rowCur, eHistoryHead.LogFileName) = UnitInfoList.LogFileName

  rowCurHistory = rowCurHistory + 1

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate

End Function

' 更新履歴シートへの履歴書込み用Function(エラー用)
' [引数]
'   index As Long            ：Unit検索で見つかったインデックス番号
'
' [戻り値]
'   無し
Public Function WriteErrorHistory(ByVal index As Long)
  Dim ws As Worksheet
  Dim rowCur As Long
  Dim i As Long

  Set ws = Worksheets(HistorySheet)
  rowCur = rowCurHistory

  ' 更新履歴シートに切替
  ws.Activate

  ' **** セルの各種設定。幅はまとめて自動調整する。****
  For i = eHistoryHead.UpdateSheet To eHistoryHead.LogFileName
    ' 罫線を引く
    Range(ws.Cells(rowCur, i).Address).Borders.LineStyle = True
    ' 表示形式を「文字列」に設定
    Range(ws.Cells(rowCur, i).Address).NumberFormatLocal = "@"
    ' フォント種別を指定
    Range(ws.Cells(rowCur, i).Address).Font.Name = "Meiryo UI"
    ' フォントサイズを指定
    Range(ws.Cells(rowCur, i).Address).Font.Size = 9
  Next i

  ' Update sheetを記入
  ws.Cells(rowCur, eHistoryHead.UpdateSheet) = UnitInfoList.SheetName
  ' Item Nameを記入
  ws.Cells(rowCur, eHistoryHead.ItemName) = UnitInfoList.ItemName
  ' Serial numberを記入
  ws.Cells(rowCur, eHistoryHead.SerialNum) = UnitInfoList.SerialNum(index)
  ' Issue numberを記入
  ws.Cells(rowCur, eHistoryHead.IssueNum) = UnitInfoList.IssueNum(index)
  ' Update Itemを記入
  ws.Cells(rowCur, eHistoryHead.UpdateItem) = UnitInfoList.UpdateItem
  ' Pre Valueを記入
  ws.Cells(rowCur, eHistoryHead.PreValue) = UnitInfoList.PreValue
  ' Update Valueを記入
  ws.Cells(rowCur, eHistoryHead.UpdateValue) = UnitInfoList.UpValue
  ' Update Statusを記入
  ws.Cells(rowCur, eHistoryHead.UpdateStatus) = UnitInfoList.HisStatus
  ' 背景色を黄色に設定
  Range(ws.Cells(rowCur, eHistoryHead.UpdateStatus).Address).Interior.Color = _
  RGB(255, 255, 0)
  ' 文字色を赤色に設定
  Range(ws.Cells(rowCur, eHistoryHead.UpdateStatus).Address).Font.Color = _
  RGB(255, 0, 0)
  ' Logfile nameを記入
  ws.Cells(rowCur, eHistoryHead.LogFileName) = UnitInfoList.LogFileName

  rowCurHistory = rowCurHistory + 1

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate

End Function

' 検索キーを使ってログを検索し、値を取得するFunction
' (DeviceType = C-BLADE/SFP/ACO用)
' [引数]
'   UnitIndex As Long       ：Unit検索時に見つかったUnitのindex番号
'   strSearchKey As String  ：検索キー(文字列)
'   strTempSheet As String  ：作業用シート名
'
' [戻り値]
'   Boolean：True  (ログから指定要素の取得に成功)
'          ：False (ログから指定要素の取得に失敗)
Public Function FindTargetItemFromLog(ByVal UnitIndex As Long, _
                                      ByVal strSearchKey As String, _
                                      ByVal strTempSheet As String) As Boolean
  Dim FoundCell As Range
  Dim MidPos As Long

  Debug.Print "(FindTargetItemFromLog):UnitIndex = " & UnitIndex & _
              " strSearchKey = " & strSearchKey & _
              " strTempSheet = " & strTempSheet

  ' ログ一時出力シートに切替
  Worksheets(strTempSheet).Activate

  Select Case (strSearchKey)
    ' 特殊処理(Unit基本情報):IssueNumberの取得処理
    Case CMN_ISSUE_NUMBER
      ' あらかじめ取得しておいた値を更新値として設定する
      UnitInfoList.UpValue = UnitInfoList.IssueNum(UnitIndex)
    ' 特殊処理(Unit基本情報):fcNumberの取得処理
    Case CMN_FC_NUMBER
      ' あらかじめ取得しておいた値を更新値として設定する
      UnitInfoList.UpValue = UnitInfoList.FcNum(UnitIndex)
    ' 特殊処理(Unit基本情報):cleiの取得処理
    Case CMN_CLEI
      ' あらかじめ取得しておいた値を更新値として設定する
      UnitInfoList.UpValue = UnitInfoList.Clei(UnitIndex)
    ' 特殊処理(Unit基本情報):vendorNameの取得処理
    Case CMN_VENDOR_NAME
      ' あらかじめ取得しておいた値を更新値として設定する
      UnitInfoList.UpValue = UnitInfoList.VendorName(UnitIndex)
    ' 特殊処理(Unit基本情報):domの取得処理
    Case CMN_DOM
      ' あらかじめ取得しておいた値を更新値として設定する
      UnitInfoList.UpValue = UnitInfoList.Dom(UnitIndex)
    ' 通常処理(検索キーによる値取得)
    Case Else
      ' 検索キーのみを使用して値を取得(検索開始セル指定無し)
      Set FoundCell = Columns(1).Find(What:=strSearchKey, LookAt:=xlPart)
      If FoundCell Is Nothing Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        FindTargetItemFromLog = False
        Exit Function
      End If

      UnitInfoList.UpValue = FoundCell.Value
      ' Debug.Print "(FindTargetItemFromLog):FoundCell.Value = " & FoundCell.Value

      ' strSearchKeyより右側の文字列を抜き出す
      MidPos = InStr(UnitInfoList.UpValue, strSearchKey) + Len(strSearchKey)
      UnitInfoList.UpValue = Mid(UnitInfoList.UpValue, MidPos)
  End Select

  Debug.Print "(FindTargetItemFromLog):UnitInfoList.UpValue = " & UnitInfoList.UpValue

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate
  FindTargetItemFromLog = True

End Function

' 検索キーを使ってログを検索し、値を取得するFunction
' (DeviceType = BLADE用)
' [引数]
'   UnitIndex As Long       ：Unit検索時に見つかったUnitのindex番号
'   strSearchKey As String  ：検索キー(文字列)
'   strTempSheet As String  ：作業用シート名
'
' [戻り値]
'   Boolean：True  (ログから指定要素の取得に成功)
'          ：False (ログから指定要素の取得に失敗)
Public Function FindTargetBladeFromLog(ByVal UnitIndex As Long, _
                                       ByVal strSearchKey As String, _
                                       ByVal strTempSheet As String) As Boolean
  Dim FoundCell As Range
  Dim FoundCell1st As Range
  Dim FoundCell2nd As Range
  Dim MidPos As Long
  Dim strFwInfo As String

  Debug.Print "(FindTargetBladeFromLog):UnitIndex = " & UnitIndex & _
              " strSearchKey = " & strSearchKey & _
              " strTempSheet = " & strTempSheet

  ' ログ一時出力シートに切替
  Worksheets(strTempSheet).Activate

  Select Case strSearchKey
    ' 特殊処理(Unit基本情報):IssueNumberの取得処理
    Case CMN_ISSUE_NUMBER
      ' あらかじめ取得しておいた値を更新値として設定する
      UnitInfoList.UpValue = UnitInfoList.IssueNum(UnitIndex)
    ' 特殊処理(Unit基本情報):fcNumberの取得処理
    Case CMN_FC_NUMBER
      ' あらかじめ取得しておいた値を更新値として設定する
      UnitInfoList.UpValue = UnitInfoList.FcNum(UnitIndex)
    ' 特殊処理(Unit基本情報):cleiの取得処理
    Case CMN_CLEI
      ' あらかじめ取得しておいた値を更新値として設定する
      UnitInfoList.UpValue = UnitInfoList.Clei(UnitIndex)
    ' 特殊処理(Unit基本情報):vendorNameの取得処理
    Case CMN_VENDOR_NAME
      ' あらかじめ取得しておいた値を更新値として設定する
      UnitInfoList.UpValue = UnitInfoList.VendorName(UnitIndex)
    ' 特殊処理(Unit基本情報):domの取得処理
    Case CMN_DOM
      ' あらかじめ取得しておいた値を更新値として設定する
      UnitInfoList.UpValue = UnitInfoList.Dom(UnitIndex)
    ' 特殊処理(Blade):U-BOOT(bootVersion)の取得処理
    Case BLADE_UNIQUE_PROCESS_01
      ' "fw-info x" (x = 対応するshelf番号)で検索する
      strFwInfo = FW_INFO & UnitInfoList.ShelfNum(UnitIndex)
      Set FoundCell1st = Columns(1).Find(What:=strFwInfo, _
                                         LookAt:=xlPart)
      If FoundCell1st Is Nothing Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        FindTargetBladeFromLog = False
        Exit Function
      End If

      ' "fw-info x"があるセルを開始セルに指定し、"bootVersion"を検索する
      Set FoundCell2nd = Columns(1).Find(What:=BOOT_VER, _
                                         After:=FoundCell1st.Cells, _
                                         LookAt:=xlPart)
      If FoundCell2nd Is Nothing Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        FindTargetBladeFromLog = False
        Exit Function
      End If

      UnitInfoList.UpValue = FoundCell2nd.Value
      ' Debug.Print "(FindTargetBladeFromLog):FoundCell2nd.Value = " & FoundCell2nd.Value

      ' "bootVersion"より右側の文字列を抜き出す
      MidPos = InStr(UnitInfoList.UpValue, BOOT_VER) + Len(BOOT_VER)
      UnitInfoList.UpValue = Mid(UnitInfoList.UpValue, MidPos)
      ' 文字列から空白を削除する
      UnitInfoList.UpValue = Replace(UnitInfoList.UpValue, " ", "")

    ' 特殊処理(Blade):expBootVersionの取得処理
    Case BLADE_UNIQUE_PROCESS_02
      ' "fw-info x" (x = 対応するshelf番号)で検索する
      strFwInfo = FW_INFO & UnitInfoList.ShelfNum(UnitIndex)
      Set FoundCell1st = Columns(1).Find(What:=strFwInfo, _
                                         LookAt:=xlPart)
      If FoundCell1st Is Nothing Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        FindTargetBladeFromLog = False
        Exit Function
      End If

      ' "fw-info x"があるセルを開始セルに指定し、"expBootVersion"を検索する
      Set FoundCell2nd = Columns(1).Find(What:=EXT_BOOT_VER, _
                                         After:=FoundCell1st.Cells, _
                                         LookAt:=xlPart)
      If FoundCell2nd Is Nothing Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        FindTargetBladeFromLog = False
        Exit Function
      End If

      UnitInfoList.UpValue = FoundCell2nd.Value
      ' Debug.Print "(FindTargetBladeFromLog):FoundCell2nd.Value = " & FoundCell2nd.Value

      ' "expBootVersion"より右側の文字列を抜き出す
      MidPos = InStr(UnitInfoList.UpValue, EXT_BOOT_VER) + Len(EXT_BOOT_VER)
      UnitInfoList.UpValue = Mid(UnitInfoList.UpValue, MidPos)
      ' 文字列から空白を削除する
      UnitInfoList.UpValue = Replace(UnitInfoList.UpValue, " ", "")

    ' 通常処理(検索キーによる値取得)
    Case Else
      Select Case UnitInfoList.ShelfConf
        Case STANDALONE_MODE
          ' **** Shelf番号によるDIP切替無し ****
          ' 検索キーのみを使用して値を取得(検索開始セル指定無し)
          Set FoundCell = Columns(1).Find(What:=strSearchKey, _
                                          LookAt:=xlPart)
        Case AGGREGATOR_MODE
          ' **** Shelf番号によるDIP切替有り ****
          ' 検索キーのみを使用して値を取得(検索開始セル指定有り)
          Set FoundCell = Columns(1).Find(What:=strSearchKey, _
                                          After:=UnitInfoList.RngDipSSH(UnitIndex).Cells, _
                                          LookAt:=xlPart)
          If FoundCell Is Nothing Then
            ' 元のシートに切替
            Worksheets(UpdateListSheet).Activate
            FindTargetBladeFromLog = False
            Exit Function
          End If
      End Select

      UnitInfoList.UpValue = FoundCell.Value
      ' Debug.Print "(FindTargetBladeFromLog):FoundCell.Value = " & FoundCell.Value

      ' strSearchKeyより右側の文字列を抜き出す
      MidPos = InStr(UnitInfoList.UpValue, strSearchKey) + Len(strSearchKey)
      UnitInfoList.UpValue = Mid(UnitInfoList.UpValue, MidPos)
  End Select

  Debug.Print "(FindTargetBladeFromLog):UnitInfoList.UpValue = " & UnitInfoList.UpValue

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate
  FindTargetBladeFromLog = True

End Function

' 検索キー、検索キーオプションを使ってログを検索し、値を取得するFunction
' (DeviceType = PIU用)
' [引数]
'   UnitIndex As Long       ：Unit検索時に見つかったUnitのindex番号
'   strSearchKey As String  ：検索キー(文字列)
'   strSearchOpKey As String：検索キーオプション(文字列)
'   strTempSheet As String  ：作業用シート名
'
' [戻り値]
'   Boolean：True  (ログから指定要素の取得に成功)
'          ：False (ログから指定要素の取得に失敗)
' [改版履歴]
'   UNIQUE_PROCESS_01の処理変更(Shelf/Slot番号の２つで判定する様に変更)
Public Function FindTargetPiuFromLog(ByVal UnitIndex As Long, _
                                     ByVal strSearchKey As String, _
                                     ByVal strSearchOpKey As String, _
                                     ByVal strTempSheet As String) As Boolean
  Dim FoundCell1st As Range
  Dim FoundCell2nd As Range
  Dim NextCell As Range
  Dim RExp As Variant
  Dim strPattern As String
  Dim SearchRng As Range
  Dim UnitRng() As Range
  Dim ShelfNum() As String
  Dim SlotNum() As String
  Dim FindNum As Long
  Dim i As Long
  Dim MidPos As Long
  Dim varKey As Variant
  Dim strCurKey As String
  Dim varOpKey As Variant
  Dim str1stOpKey As String
  Dim str2ndOpKey As String
  Dim row1st As Long
  Dim row2nd As Long
  Dim rowNext As Long
  Dim rowDipEnd As Long
  Dim strLen As Long

  Debug.Print "(FindTargetPiuFromLog):UnitIndex = " & UnitIndex & _
              " strSearchKey = " & strSearchKey & _
              " strSearchOpKey = " & strSearchOpKey & _
              " strTempSheet = " & strTempSheet

  ' ログ一時出力シートに切替
  Worksheets(strTempSheet).Activate

  ' 検索キーオプションに指定が無い場合、検索キーのみを使用して値を取得
  If (strSearchOpKey = "") Then
    Select Case strSearchKey
    ' 特殊処理(Unit基本情報):IssueNumberの取得処理
    Case CMN_ISSUE_NUMBER
      ' あらかじめ取得しておいた値を更新値として設定する
      UnitInfoList.UpValue = UnitInfoList.IssueNum(UnitIndex)
    ' 特殊処理(Unit基本情報):fcNumberの取得処理
    Case CMN_FC_NUMBER
      ' あらかじめ取得しておいた値を更新値として設定する
      UnitInfoList.UpValue = UnitInfoList.FcNum(UnitIndex)
    ' 特殊処理(Unit基本情報):cleiの取得処理
    Case CMN_CLEI
      ' あらかじめ取得しておいた値を更新値として設定する
      UnitInfoList.UpValue = UnitInfoList.Clei(UnitIndex)
    ' 特殊処理(Unit基本情報):vendorNameの取得処理
    Case CMN_VENDOR_NAME
      ' あらかじめ取得しておいた値を更新値として設定する
      UnitInfoList.UpValue = UnitInfoList.VendorName(UnitIndex)
    ' 特殊処理(Unit基本情報):domの取得処理
    Case CMN_DOM
      ' あらかじめ取得しておいた値を更新値として設定する
      UnitInfoList.UpValue = UnitInfoList.Dom(UnitIndex)
    ' 特殊処理(PIU) : PIUに搭載されているCFP2のSerial Numberを取得する
    Case PIU_UNIQUE_PROCESS_01
      ' (1)unitName = CFP2の行を検索する
      Set RExp = CreateObject("VBScript.RegExp")
      strPattern = LOG_CFP2 & "$"  ' UnitNameにメタ文字(文字列の末尾)を追加
      ' 動的配列の初期化
      FindNum = 0
      ReDim UnitRng(FindNum)
      ReDim ShelfNum(FindNum)
      ReDim SlotNum(FindNum)

      With RExp
        .Pattern = strPattern  ' 検索パターンを設定
        .Global = True         ' 文字列全体を検索

        For Each SearchRng In ActiveSheet.UsedRange
          If .Test(SearchRng.Formula) Then
            ' 検索Hitした個数をインクリメント
            FindNum = FindNum + 1
            ' 動的配列を再定義
            ReDim Preserve UnitRng(FindNum)
            ReDim ShelfNum(FindNum)
            ReDim SlotNum(FindNum)
            ' 対応する配列に検索HitしたRangeオブジェクトを格納
            Set UnitRng(FindNum) = SearchRng
          End If
        Next SearchRng
      End With

      Set RExp = Nothing

      If (FindNum = 0) Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        Debug.Print "(FindTargetPiuFromLog):" & LOG_CFP2 & " is not found."
        FindTargetPiuFromLog = False
        Exit Function
      End If

      Debug.Print "(FindTargetPiuFromLog):FindNum = " & FindNum

      ' (2)検索Hitした行数から、逆方向でinventoryNameを検索し、Shelf/slot番号が同じ
      '    ものを特定する
      For i = 1 To FindNum
        Set FoundCell1st = Columns(1).Find(What:=LOG_INVENTORY_NAME, _
                                           After:=UnitRng(i).Cells, _
                                           LookAt:=xlPart, _
                                           SearchDirection:=xlPrevious)
        If FoundCell1st Is Nothing Then
          ' 元のシートに切替
          Worksheets(UpdateListSheet).Activate
          Debug.Print "(FindTargetPiuFromLog):" & LOG_INVENTORY_NAME & " is not found."
          FindTargetPiuFromLog = False
          Exit Function
        End If

        ShelfNum(i) = FoundCell1st.Value
        SlotNum(i) = FoundCell1st.Value
        'Debug.Print "(FindTargetPiuFromLog):FoundCell1st.Value = " & FoundCell1st.Value
        
        ' [例] inventoryName  "Shelf: 1 slot: 0 subslot: 0 port: 0"
        ' **** Shelf番号取得処理 ****
        ' "Shelf:"より右側の文字列を抜き出す
        MidPos = InStr(ShelfNum(i), LOG_SHELF) + Len(LOG_SHELF)
        ShelfNum(i) = Mid(ShelfNum(i), MidPos)

        ' 文字列先頭から"slot:"の直前までの文字列を抜き出す
        strLen = InStr(ShelfNum(i), LOG_SLOT) - 1
        ShelfNum(i) = Left(ShelfNum(i), strLen)

        ' 文字列から空白を削除
        ShelfNum(i) = Replace(ShelfNum(i), " ", "")

        ' **** Slot番号取得処理 ****
        ' "slot:"より右側の文字列を抜き出す
        MidPos = InStr(SlotNum(i), LOG_SLOT) + Len(LOG_SLOT)
        SlotNum(i) = Mid(SlotNum(i), MidPos)

        ' 文字列先頭から"subslot:"の直前までの文字列を抜き出す
        strLen = InStr(SlotNum(i), LOG_SUB_SLOT) - 1
        SlotNum(i) = Left(SlotNum(i), strLen)

        ' 文字列から空白を削除
        SlotNum(i) = Replace(SlotNum(i), " ", "")
        Debug.Print "(FindTargetPiuFromLog):ShelfNum(" & i & ") = " & ShelfNum(i) & _
                    " SlotNum(" & i & ") = " & SlotNum(i)

        ' Shelf/Slot番号が該当PIUと同じであれば、Serial Numberを検索する
        If (ShelfNum(i) = UnitInfoList.ShelfNum(UnitIndex)) And _
           (SlotNum(i) = UnitInfoList.SlotNum(UnitIndex)) Then
          Set FoundCell2nd = Columns(1).Find(What:=LOG_SERIAL_NUM, _
                                             After:=UnitRng(i).Cells, _
                                             LookAt:=xlPart)
          If FoundCell2nd Is Nothing Then
            ' 元のシートに切替
            Worksheets(UpdateListSheet).Activate
            Debug.Print "(FindTargetPiuFromLog):" & LOG_SERIAL_NUM & " is not found."
            FindTargetPiuFromLog = False
            Exit Function
          End If

          ' Serial Numberが検索Hitしたら、更新値として保存する
          UnitInfoList.UpValue = FoundCell2nd.Value
          ' Serial Numberより右側の文字列を抜き出す
          MidPos = InStr(UnitInfoList.UpValue, LOG_SERIAL_NUM) + Len(LOG_SERIAL_NUM)
          UnitInfoList.UpValue = Mid(UnitInfoList.UpValue, MidPos)
          ' 文字列から空白を削除
          UnitInfoList.UpValue = Replace(UnitInfoList.UpValue, " ", "")
          Exit For
        End If
      Next i

    Case Else
      ' 検索キーがカンマ区切りで複数指定されているか確認
      varKey = Split(strSearchKey, ",")

      ' カンマ区切りで無い場合
      If (LBound(varKey) = UBound(varKey)) Then
        strCurKey = varKey(0)
      ' カンマ区切りの場合
      Else
        For i = 0 To UBound(varKey)
          Debug.Print "(FindTargetPiuFromLog):varKey(" & i & ") = " & varKey(i)
          ' slot番号に対応する検索キーを取得
          If (i = Val(UnitInfoList.SlotNum(UnitIndex)) - 1) Then
            strCurKey = varKey(i)
            Exit For
          End If
        Next i
      End If

      Select Case UnitInfoList.ShelfConf
        Case STANDALONE_MODE
          ' **** Shelf番号によるDIP切替無し ****
          ' 検索キーのみを使用して値を取得(検索開始セル指定無し)
          Set FoundCell1st = Columns(1).Find(What:=strCurKey, LookAt:=xlPart)
        Case AGGREGATOR_MODE
          ' **** Shelf番号によるDIP切替有り ****
          ' 検索キーのみを使用して値を取得(検索開始セル指定有り)
          Set FoundCell1st = Columns(1).Find(What:=strCurKey, _
                                             After:=UnitInfoList.RngDipSSH(UnitIndex).Cells, _
                                             LookAt:=xlPart)
      End Select

      If FoundCell1st Is Nothing Then
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        Debug.Print "(FindTargetPiuFromLog):" & strCurKey & " is not found."
        FindTargetPiuFromLog = False
        Exit Function
      End If

      UnitInfoList.UpValue = FoundCell1st.Value
      ' Debug.Print "(FindTargetPiuFromLog):FoundCell1st.Value = " & FoundCell1st.Value

      ' strCurKeyより右側の文字列を抜き出す
      MidPos = InStr(UnitInfoList.UpValue, strCurKey) + Len(strCurKey)
      UnitInfoList.UpValue = Mid(UnitInfoList.UpValue, MidPos)
    End Select

    Debug.Print "(FindTargetPiuFromLog):UnitInfoList.UpValue = " & UnitInfoList.UpValue
  ' 検索キーオプションに指定が有る場合、初めに検索キーオプションで検索後、
  ' 検索キーを使用して値を取得する
  Else
    ' 検索キーがカンマ区切りで複数指定されているか確認
    varOpKey = Split(strSearchOpKey, ",")

    ' カンマ区切りで無い場合
    If (LBound(varOpKey) = UBound(varOpKey)) Then
      str1stOpKey = varOpKey(0)
    ' カンマ区切りの場合
    Else
      For i = 0 To UBound(varOpKey)
        Debug.Print "(FindTargetPiuFromLog):varOpKey(" & i & ") = " & varOpKey(i)
        ' slot番号に対応する検索キーを取得
        If (i = Val(UnitInfoList.SlotNum(UnitIndex)) - 1) Then
          str1stOpKey = varOpKey(i)

          ' 最後尾のSlotで無い場合、次Slotの検索キーオプションも取得しておく
          ' *** 以下の様に、装置によってMax Slotが異なる事に注意 ***
          ' T500(Max Slot = 4), T510(Max Slot = 2),
          ' T300(Max Slot = 5), T310(Max Slot = 2)
          If (i <> UBound(varOpKey)) Then
            str2ndOpKey = varOpKey(i + 1)
          End If

          Exit For
        End If
      Next i
    End If

    ' 指定された検索キーオプションで検索
    Select Case UnitInfoList.ShelfConf
      Case STANDALONE_MODE
        ' **** Shelf番号によるDIP切替無し ****
        ' 検索キーのみを使用して値を取得(検索開始セル指定無し)
        Set FoundCell1st = Columns(1).Find(What:=str1stOpKey, LookAt:=xlPart)
      Case AGGREGATOR_MODE
        ' **** Shelf番号によるDIP切替有り ****
        ' 検索キーのみを使用して値を取得(検索開始セル指定有り)
        Set FoundCell1st = Columns(1).Find(What:=str1stOpKey, _
                                           After:=UnitInfoList.RngDipSSH(UnitIndex).Cells, _
                                           LookAt:=xlPart)
    End Select

    If FoundCell1st Is Nothing Then
      ' 元のシートに切替
      Worksheets(UpdateListSheet).Activate
      Debug.Print "(FindTargetPiuFromLog):" & str1stOpKey & " is not found."
      FindTargetPiuFromLog = False
      Exit Function
    End If

    row1st = FoundCell1st.Row
    ' Debug.Print "(FindTargetPiuFromLog):FoundCell1st.Value = " & FoundCell1st.Value

    ' 最後尾のUnitで無い場合、次Slotの検索キーオプション行を取得する
    If (str2ndOpKey <> "") Then
      Select Case UnitInfoList.ShelfConf
        Case STANDALONE_MODE
          ' **** Shelf番号によるDIP切替無し ****
          ' 検索キーのみを使用して値を取得(検索開始セル指定無し)
          Set FoundCell2nd = Columns(1).Find(What:=str2ndOpKey, LookAt:=xlPart)
        Case AGGREGATOR_MODE
          ' **** Shelf番号によるDIP切替有り ****
          ' 検索キーのみを使用して値を取得(検索開始セル指定有り)
          Set FoundCell2nd = Columns(1).Find(What:=str2ndOpKey, _
                                             After:=UnitInfoList.RngDipSSH(UnitIndex).Cells, _
                                             LookAt:=xlPart)
      End Select

      ' 次Slotの検索キーオプションが見つからなかった場合、エラーとはせずに
      ' Max Slot数の少ないT510 or T310であると判断し、最後尾のUnit扱いとする。
      ' T500(Max Slot:4), T510(Max Slot:2), T300(Max Slot:5), T310(Max Slot:2)
      If FoundCell2nd Is Nothing Then
        ' 最後尾のUnit扱いとするため、str2ndOpKeyを初期化する
        str2ndOpKey = ""
      Else
        row2nd = FoundCell2nd.Row
        ' Debug.Print "(FindTargetPiuFromLog):FoundCell2nd.Value = " & FoundCell2nd.Value
      End If
    End If

    ' 検索キーオプション行から、検索キーで検索
    Set NextCell = Columns(1).Find(What:=strSearchKey, After:=FoundCell1st.Cells, _
                                   LookAt:=xlPart)

    If NextCell Is Nothing Then
      ' 元のシートに切替
      Worksheets(UpdateListSheet).Activate
      Debug.Print "(FindTargetPiuFromLog):" & strSearchKey & " is not found."
      FindTargetPiuFromLog = False
      Exit Function
    End If

    rowNext = NextCell.Row
    ' Debug.Print "(FindTargetPiuFromLog):NextCell.Value = " & NextCell.Value

    Debug.Print "(FindTargetPiuFromLog):row1st = " & row1st & _
                " row2nd = " & row2nd & " rowNext = " & rowNext

    ' **** 取得値の正当性チェック ****
    ' 最後尾のSlotで無い場合、検索キーでHitした行数が、検索キーオプション行と、
    ' 次Slotの検索キーオプション行との間にある事を確認
    If (str2ndOpKey <> "") Then
      If (rowNext > row1st) And (rowNext < row2nd) Then
        UnitInfoList.UpValue = NextCell.Value
      Else
        ' 元のシートに切替
        Worksheets(UpdateListSheet).Activate
        Debug.Print "(FindTargetPiuFromLog):UpValue is not found(1)."
        FindTargetPiuFromLog = False
        Exit Function
      End If
    ' 最後尾のSlotの場合、検索キーでHitした行数が、検索キーオプション行より
    ' 大きい行数である事を確認
    Else
      Select Case UnitInfoList.ShelfConf
        Case STANDALONE_MODE
          ' Standalone構成の場合、以下条件であることをチェック
          ' *** 検索キーオプション行 < 検索キーHit行 ***
          If (rowNext > row1st) Then
            UnitInfoList.UpValue = NextCell.Value
          Else
            ' 元のシートに切替
            Worksheets(UpdateListSheet).Activate
            Debug.Print "(FindTargetPiuFromLog):UpValue is not found(2)."
            FindTargetPiuFromLog = False
            Exit Function
          End If
        Case AGGREGATOR_MODE
          ' Aggregator構成の場合、以下条件であることをチェック
          ' *** 検索キーオプション行 < 検索キーHit行 < Dip切替終了行 ***
          If (rowNext > row1st) And _
             (rowNext < UnitInfoList.RngDipEnd(UnitIndex).Row) Then
            UnitInfoList.UpValue = NextCell.Value
          Else
            ' 元のシートに切替
            Worksheets(UpdateListSheet).Activate
            Debug.Print "(FindTargetPiuFromLog):UpValue is not found(3)."
            FindTargetPiuFromLog = False
            Exit Function
          End If
      End Select
    End If

    ' strSearchKeyより右側の文字列を抜き出す
    MidPos = InStr(UnitInfoList.UpValue, strSearchKey) + Len(strSearchKey)
    UnitInfoList.UpValue = Mid(UnitInfoList.UpValue, MidPos)
    Debug.Print "(FindTargetPiuFromLog):UnitInfoList.UpValue = " & UnitInfoList.UpValue

  End If

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate
  FindTargetPiuFromLog = True

End Function

' 管理簿の物品を更新するためのFunction
' [引数]
'   strDeviceType As String  ：内部処理用のデバイスタイプ名
'   rowNum As Long           ：「Update_List」シート上の現在行
'   UnitIndex As Long        ：検索で見つかったUnitのindex番号
'   strUnitName As String    ：「unitName (at Log)」に記載のUnit名
'   strTempSheet As String   ：作業用シート名
'
' [戻り値]
'   Boolean：True  (管理簿の物品更新に成功)
'          ：False (管理簿の物品更新に失敗)
Public Function UpdateExistItem(ByVal strDeviceType As String, _
                                ByVal rowNum As Long, _
                                ByVal UnitIndex As Long, _
                                ByVal strUnitName As String, _
                                ByVal strTempSheet As String) As Boolean

  ' DeviceTypeによって処理分け
  Select Case (strDeviceType)
    Case BLADE_DEVICE
      If (UnitInfoList.ShelfConf = AGGREGATOR_MODE) Then
        ' ログ上で該当Unitの情報を取得できるかをチェック
        ' (" ssh -p 6022 fujitsu@127.1.1." + Shelf番号を検索)
        If (CheckDipSSHFromLog(UnitIndex, TEMP_SHEET_NAME) = False) Then
          ' チェックNGの場合、更新履歴のUpdate Statusをエラーにして、
          ' 該当Unitの処理SKIP
          UnitInfoList.HisStatus = "ログ上に、" & strUnitName & _
                                   "の搭載Shelfに対応するDIP接続ログ" & vbCrLf & _
                                   "[" & DIP_SSH & UnitInfoList.ShelfNum(UnitIndex) & "]" & _
                                   vbCrLf & _
                                   "がありませんでした。ログを確認して下さい。"
          Call WriteErrorHistory(UnitIndex)

          UpdateExistItem = False
          Exit Function
        End If
      End If

      Call UpdateBladeInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

    Case C_BLADE_DEVICE
      Call UpdateCBladeInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

    Case PIU_DEVICE
      If (UnitInfoList.ShelfConf = AGGREGATOR_MODE) Then
        ' ログ上で該当Unitの情報を取得できるかをチェック
        ' (" ssh -p 6022 fujitsu@127.1.1." + Shelf番号を検索)
        If (CheckDipSSHFromLog(UnitIndex, TEMP_SHEET_NAME) = False) Then
          ' チェックNGの場合、更新履歴のUpdate Statusをエラーにして、
          ' 該当Unitの処理SKIP
          UnitInfoList.HisStatus = "ログ上に、" & strUnitName & _
                                   "の搭載Shelfに対応するDIP接続ログ" & vbCrLf & _
                                   "[" & DIP_SSH & UnitInfoList.ShelfNum(UnitIndex) & "]" & _
                                   vbCrLf & _
                                   "がありませんでした。ログを確認して下さい。"
          Call WriteErrorHistory(UnitIndex)

          UpdateExistItem = False
          Exit Function
        End If
      End If

      Call UpdatePiuInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

    Case SFP_DEVICE
      Call UpdateSfpInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

    Case ACO_DEVICE
      Call UpdateAcoInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

  End Select

  UpdateExistItem = True

End Function

' 基本の物品Update用Function(特殊処理無し)
' [引数]
'   rowNum As Long           ：「Update_List」シート上の現在行
'   UnitIndex As Long        ：検索で見つかったUnitのindex番号
'   strUnitName As String    ：「unitName (at Log)」に記載のUnit名
'   strTempSheet As String   ：作業用シート名
'
' [戻り値]
'   無し
Public Function UpdateBasicInfo(ByVal rowNum As Long, _
                                ByVal UnitIndex As Long, _
                                ByVal strUnitName As String, _
                                ByVal strTempSheet As String)
  Dim i As Long
  Dim rowCur As Long
  Dim Unitws As Worksheet
  Dim strComposite As String
  Dim ItemDic As Object
  Dim vPostscriptVal As Variant
  Dim vSearchKey As Variant
  Dim vElement As Variant
  Dim CompositeVal As String
  Dim str2ndElement As String

  Debug.Print "(UpdateBasicInfo):rowNum = " & rowNum & _
              " UnitIndex = " & UnitIndex & _
              " strUnitName = " & strUnitName & _
              " strTempSheet = " & strTempSheet

  rowCur = rowNum
  Set Unitws = Worksheets(UnitInfoList.SheetName)
  Set ItemDic = CreateObject("Scripting.Dictionary")

  ' 「unitName (at Log)」の記載が、現在処理中のUnitNameと異なる行に
  ' なるまで繰り返す
  Do Until ((Cells(rowCur, ColList.colUnitName) <> "") And _
            (Cells(rowCur, ColList.colUnitName) <> strUnitName)) Or _
           (rowCur > rowLastUpdateList)
    UnitInfoList.UpdateItem = Cells(rowCur, ColList.colUpdateItem)
    UnitInfoList.SearchKey = Cells(rowCur, ColList.colSearchKey)
    UnitInfoList.SearchKeyOp = Cells(rowCur, ColList.colSearchKeyOption)
    strComposite = Cells(rowCur, ColList.colComposite)
 
    Debug.Print "(UpdateBasicInfo):UpdateItem = " & UnitInfoList.UpdateItem & _
                " SearchKey = " & UnitInfoList.SearchKey & _
                " SearchKeyOp = " & UnitInfoList.SearchKeyOp

    ' 更新対象項目の列を特定
    If (FindHeadColFromList(UnitInfoList.SheetName, _
                            UnitInfoList.UpdateItem, _
                            UnitInfoList.colUpdateItem) = False) Then
      ' 特定できない場合、その旨を更新履歴に残す
      UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                               UnitInfoList.UpdateItem & "の見出し列がありません"
      ' PreValue, UpValueは空にしておく
      UnitInfoList.PreValue = ""
      UnitInfoList.UpValue = ""
      Call WriteErrorHistory(UnitIndex)
      GoTo UPDATE_CHECK_SKIP
    End If

    ' 更新前の値を保存
    UnitInfoList.PreValue = Unitws.Cells(UnitInfoList.rowTargetItem, _
                                         UnitInfoList.colUpdateItem)
    Debug.Print "(UpdateBasicInfo):UnitInfoList.PreValue = " & UnitInfoList.PreValue & _
                " strComposite = " & strComposite

    ' Composite = OFFの場合(ログから検索キーを基に値を取得)
    Select Case (strComposite)
    Case COMP_OFF
      ' 更新後の値をログから取得し、UnitInfoList.UpValueに保存
      If (FindTargetItemFromLog(UnitIndex, _
                                UnitInfoList.SearchKey, _
                                strTempSheet) = False) Then
        ' ログから指定値の検索に失敗した場合、その旨を更新履歴に残す
        UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                                 UnitInfoList.UpdateItem & "がログ中にありません"
        ' UpValueは空にしておく
        UnitInfoList.UpValue = ""
        Call WriteErrorHistory(UnitIndex)
        GoTo UPDATE_CHECK_SKIP
      End If

      ' 更新後の値を、更新対象項目名とのペアで辞書に登録しておく
      ItemDic.Add UnitInfoList.UpdateItem, UnitInfoList.UpValue

      ' 更新前後の値を比較し、異なる場合のみ管理簿を更新する
      ' (更新前後で同値の場合、管理簿は更新せず、更新履歴にも残らない)
      If (UnitInfoList.PreValue <> UnitInfoList.UpValue) Then
        Unitws.Cells(UnitInfoList.rowTargetItem, _
                     UnitInfoList.colUpdateItem) = UnitInfoList.UpValue
        ' 更新ステータス = 更新成功
        UnitInfoList.HisStatus = UPDATE_SUCCESS
        Call WriteHistory(UnitIndex)
      End If

    ' Composite = ADD_IF_NE_ALLの場合
    ' (現在の最新値が更新値と異なる場合、追記する)
    Case COMP_ADD_IF_NE_ALL
      ' 先に更新後の値を辞書より作成する
      vSearchKey = Split(Cells(rowCur, ColList.colSearchKey), ",")
      For i = 0 To UBound(vSearchKey)
        If (ItemDic.Exists(vSearchKey(i)) = False) Then
          ' SearchKeyに指定した、更新対象項目名が辞書に無い場合、その旨を更新履歴に残す
          UnitInfoList.HisStatus = UpdateListSheet & "シートの" & _
                                   UnitInfoList.UpdateItem & "において、" & vbCrLf & _
                                   vSearchKey(i) & "を指定した行より後にComposite指定して下さい。"
          ' UpValueは空にしておく
          UnitInfoList.UpValue = ""
          Call WriteErrorHistory(UnitIndex)
          GoTo UPDATE_CHECK_SKIP
        End If

        If (i = UBound(vSearchKey)) Then
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i))
        Else
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i)) & ","
        End If
      Next i

      Debug.Print "(UpdateBasicInfo):CompositeVal = " & CompositeVal

      ' 更新前の値が無い("")の場合、Splitで空の配列が返るので分ける
      If (UnitInfoList.PreValue <> "") Then
        ' 更新前の値を改行(vbLF)区切りで取り出す
        vPostscriptVal = Split(UnitInfoList.PreValue, vbLf)

        ' Excelの最大LF数に達しているかをチェックする
        If (UBound(vPostscriptVal) >= MAX_LF) Then
          UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                                   UnitInfoList.UpdateItem & vbCrLf & _
                                   "の1セルあたりの改行(LF)数が多すぎるため、" & _
                                   vbCrLf & _
                                   "更新しませんでした。見直して下さい。"
          ' UpValueは空にしておく
          UnitInfoList.UpValue = ""
          Call WriteErrorHistory(UnitIndex)
          GoTo UPDATE_CHECK_SKIP
        End If

        Debug.Print "(UpdateBasicInfo):vPostscriptVal(0) = " & vPostscriptVal(0)

        ' １番最初の値が最新情報とみなし、追記する値と異なる場合のみ更新する
        If (vPostscriptVal(0) <> CompositeVal) Then
          UnitInfoList.UpValue = CompositeVal & vbLf & UnitInfoList.PreValue
        Else
          GoTo UPDATE_CHECK_SKIP
        End If
      ' 更新前の値が無い("")ため、新規の値で直接更新する
      Else
        UnitInfoList.UpValue = CompositeVal
      End If

      Debug.Print "(UpdateBasicInfo):UnitInfoList.UpValue = " & UnitInfoList.UpValue
      Unitws.Cells(UnitInfoList.rowTargetItem, _
                   UnitInfoList.colUpdateItem) = UnitInfoList.UpValue
      ' 更新ステータス = 更新成功
      UnitInfoList.HisStatus = UPDATE_SUCCESS
      Call WriteHistory(UnitIndex)
    
    ' Composite = ADD_IF_NE_2NDの場合
    ' (現在の最新値の2番目の要素が、更新値の2番目の要素と異なる場合、追記する)
    Case COMP_ADD_IF_NE_2ND
      ' 先に更新後の値を辞書より作成する
      vSearchKey = Split(Cells(rowCur, ColList.colSearchKey), ",")
      For i = 0 To UBound(vSearchKey)
        If (ItemDic.Exists(vSearchKey(i)) = False) Then
          ' SearchKeyに指定した、更新対象項目名が辞書に無い場合、その旨を更新履歴に残す
          UnitInfoList.HisStatus = UpdateListSheet & "シートの" & _
                                   UnitInfoList.UpdateItem & "において、" & vbCrLf & _
                                   vSearchKey(i) & "を指定した行より後にComposite指定して下さい。"
          ' UpValueは空にしておく
          UnitInfoList.UpValue = ""
          Call WriteErrorHistory(UnitIndex)
          GoTo UPDATE_CHECK_SKIP
        End If

        If (i = UBound(vSearchKey)) Then
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i))
        Else
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i)) & ","
        End If
      Next i

      ' 2番目の要素を取得
      str2ndElement = ItemDic.Item(vSearchKey(1))

      Debug.Print "(UpdateBasicInfo):CompositeVal = " & CompositeVal & _
                  " str2ndElement = " & str2ndElement

      ' 更新前の値が無い("")の場合、Splitで空の配列が返るので分ける
      If (UnitInfoList.PreValue <> "") Then
        ' 更新前の値を改行(vbLF)区切りで取り出す
        vPostscriptVal = Split(UnitInfoList.PreValue, vbLf)

        ' Excelの最大LF数に達しているかをチェックする
        If (UBound(vPostscriptVal) >= MAX_LF) Then
          UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                                   UnitInfoList.UpdateItem & vbCrLf & _
                                   "の1セルあたりの改行(LF)数が多すぎるため、" & _
                                   vbCrLf & _
                                   "更新しませんでした。見直して下さい。"
          ' UpValueは空にしておく
          UnitInfoList.UpValue = ""
          Call WriteErrorHistory(UnitIndex)
          GoTo UPDATE_CHECK_SKIP
        End If

        Debug.Print "(UpdateBasicInfo):vPostscriptVal(0) = " & vPostscriptVal(0)

        ' １番最初の値が最新情報とみなし、カンマ区切りの2番目の要素と、同じく
        ' 追記するカンマ区切りの2番目の要素が異なる場合のみ更新する

        ' 1番最初の要素がNull文字(改行のみ)だった場合、改行を付与せずに
        ' 無条件で更新する
        If (vPostscriptVal(0) = "") Then
          UnitInfoList.UpValue = CompositeVal & UnitInfoList.PreValue
        Else
          vElement = Split(vPostscriptVal(0), ",")
          ' カンマ区切りで無い場合、無条件で更新する
          If (LBound(vElement) = UBound(vElement)) Then
            UnitInfoList.UpValue = CompositeVal & vbLf & UnitInfoList.PreValue
          ' カンマ区切りの場合
          Else
            If (vElement(1) <> str2ndElement) Then
              UnitInfoList.UpValue = CompositeVal & vbLf & UnitInfoList.PreValue
            Else
              GoTo UPDATE_CHECK_SKIP
            End If
          End If
        End If
      ' 更新前の値が無い("")ため、新規の値で直接更新する
      Else
        UnitInfoList.UpValue = CompositeVal
      End If

      Debug.Print "(UpdateBasicInfo):UnitInfoList.UpValue = " & UnitInfoList.UpValue
      Unitws.Cells(UnitInfoList.rowTargetItem, _
                   UnitInfoList.colUpdateItem) = UnitInfoList.UpValue
      ' 更新ステータス = 更新成功
      UnitInfoList.HisStatus = UPDATE_SUCCESS
      Call WriteHistory(UnitIndex)
    End Select

UPDATE_CHECK_SKIP:
    rowCur = rowCur + 1
  Loop

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate

End Function

' DeviceType = BLADEの場合の物品Update用Function
' [引数]
'   rowNum As Long           ：「Update_List」シート上の現在行
'   UnitIndex As Long        ：検索で見つかったUnitのindex番号
'   strUnitName As String    ：「unitName (at Log)」に記載のUnit名
'   strTempSheet As String   ：作業用シート名
'
' [戻り値]
'   無し
Public Function UpdateBladeInfo(ByVal rowNum As Long, _
                                ByVal UnitIndex As Long, _
                                ByVal strUnitName As String, _
                                ByVal strTempSheet As String)
  Dim i As Long
  Dim rowCur As Long
  Dim Unitws As Worksheet
  Dim strComposite As String
  Dim ItemDic As Object
  Dim vPostscriptVal As Variant
  Dim vSearchKey As Variant
  Dim vElement As Variant
  Dim CompositeVal As String
  Dim str2ndElement As String

  Debug.Print "(UpdateBladeInfo):rowNum = " & rowNum & _
              " UnitIndex = " & UnitIndex & _
              " strUnitName = " & strUnitName & _
              " strTempSheet = " & strTempSheet

  rowCur = rowNum
  Set Unitws = Worksheets(UnitInfoList.SheetName)
  Set ItemDic = CreateObject("Scripting.Dictionary")

  ' 「unitName (at Log)」の記載が、現在処理中のUnitNameと異なる行に
  ' なるまで繰り返す
  Do Until ((Cells(rowCur, ColList.colUnitName) <> "") And _
            (Cells(rowCur, ColList.colUnitName) <> strUnitName)) Or _
           (rowCur > rowLastUpdateList)
    UnitInfoList.UpdateItem = Cells(rowCur, ColList.colUpdateItem)
    UnitInfoList.SearchKey = Cells(rowCur, ColList.colSearchKey)
    UnitInfoList.SearchKeyOp = Cells(rowCur, ColList.colSearchKeyOption)
    strComposite = Cells(rowCur, ColList.colComposite)
 
    Debug.Print "(UpdateBladeInfo):UpdateItem = " & UnitInfoList.UpdateItem & _
                " SearchKey = " & UnitInfoList.SearchKey & _
                " SearchKeyOp = " & UnitInfoList.SearchKeyOp

    ' 更新対象項目の列を特定
    If (FindHeadColFromList(UnitInfoList.SheetName, _
                            UnitInfoList.UpdateItem, _
                            UnitInfoList.colUpdateItem) = False) Then
      ' 特定できない場合、その旨を更新履歴に残す
      UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                               UnitInfoList.UpdateItem & "の見出し列がありません"
      ' PreValue, UpValueは空にしておく
      UnitInfoList.PreValue = ""
      UnitInfoList.UpValue = ""
      Call WriteErrorHistory(UnitIndex)
      GoTo UPDATE_BLADE_CHECK_SKIP
    End If

    ' 更新前の値を保存
    UnitInfoList.PreValue = Unitws.Cells(UnitInfoList.rowTargetItem, _
                                         UnitInfoList.colUpdateItem)
    Debug.Print "(UpdateBladeInfo):UnitInfoList.PreValue = " & UnitInfoList.PreValue & _
                " strComposite = " & strComposite

    ' Composite = OFFの場合(ログから検索キーを基に値を取得)
    Select Case (strComposite)
    Case COMP_OFF
      ' 更新後の値をログから取得し、UnitInfoList.UpValueに保存
      If (FindTargetBladeFromLog(UnitIndex, _
                                 UnitInfoList.SearchKey, _
                                 strTempSheet) = False) Then
        ' ログから指定値の検索に失敗した場合、その旨を更新履歴に残す
        UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                                 UnitInfoList.UpdateItem & "がログ中にありません"
        ' UpValueは空にしておく
        UnitInfoList.UpValue = ""
        Call WriteErrorHistory(UnitIndex)
        GoTo UPDATE_BLADE_CHECK_SKIP
      End If

      ' 更新後の値を、更新対象項目名とのペアで辞書に登録しておく
      ItemDic.Add UnitInfoList.UpdateItem, UnitInfoList.UpValue

      ' 更新前後の値を比較し、異なる場合のみ管理簿を更新する
      ' (更新前後で同値の場合、管理簿は更新せず、更新履歴にも残らない)
      If (UnitInfoList.PreValue <> UnitInfoList.UpValue) Then
        Unitws.Cells(UnitInfoList.rowTargetItem, _
                     UnitInfoList.colUpdateItem) = UnitInfoList.UpValue
        ' 更新ステータス = 更新成功
        UnitInfoList.HisStatus = UPDATE_SUCCESS
        Call WriteHistory(UnitIndex)
      End If

    ' Composite = ADD_IF_NE_ALLの場合
    ' (現在の最新値が更新値と異なる場合、追記する)
    Case COMP_ADD_IF_NE_ALL
      ' 先に更新後の値を辞書より作成する
      vSearchKey = Split(Cells(rowCur, ColList.colSearchKey), ",")
      For i = 0 To UBound(vSearchKey)
        If (ItemDic.Exists(vSearchKey(i)) = False) Then
          ' SearchKeyに指定した、更新対象項目名が辞書に無い場合、その旨を更新履歴に残す
          UnitInfoList.HisStatus = UpdateListSheet & "シートの" & _
                                   UnitInfoList.UpdateItem & "において、" & vbCrLf & _
                                   vSearchKey(i) & "を指定した行より後にComposite指定して下さい。"
          ' UpValueは空にしておく
          UnitInfoList.UpValue = ""
          Call WriteErrorHistory(UnitIndex)
          GoTo UPDATE_BLADE_CHECK_SKIP
        End If

        If (i = UBound(vSearchKey)) Then
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i))
        Else
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i)) & ","
        End If
      Next i

      Debug.Print "(UpdateBladeInfo):CompositeVal = " & CompositeVal

      ' 更新前の値が無い("")の場合、Splitで空の配列が返るので分ける
      If (UnitInfoList.PreValue <> "") Then
        ' 更新前の値を改行(vbLF)区切りで取り出す
        vPostscriptVal = Split(UnitInfoList.PreValue, vbLf)

        ' Excelの最大LF数に達しているかをチェックする
        If (UBound(vPostscriptVal) >= MAX_LF) Then
          UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                                   UnitInfoList.UpdateItem & vbCrLf & _
                                   "の1セルあたりの改行(LF)数が多すぎるため、" & _
                                   vbCrLf & _
                                   "更新しませんでした。見直して下さい。"
          ' UpValueは空にしておく
          UnitInfoList.UpValue = ""
          Call WriteErrorHistory(UnitIndex)
          GoTo UPDATE_BLADE_CHECK_SKIP
        End If

        Debug.Print "(UpdateBladeInfo):vPostscriptVal(0) = " & vPostscriptVal(0)

        ' １番最初の値が最新情報とみなし、追記する値と異なる場合のみ更新する
        If (vPostscriptVal(0) <> CompositeVal) Then
          UnitInfoList.UpValue = CompositeVal & vbLf & UnitInfoList.PreValue
        Else
          GoTo UPDATE_BLADE_CHECK_SKIP
        End If
      ' 更新前の値が無い("")ため、新規の値で直接更新する
      Else
        UnitInfoList.UpValue = CompositeVal
      End If

      Debug.Print "(UpdateBladeInfo):UnitInfoList.UpValue = " & UnitInfoList.UpValue
      Unitws.Cells(UnitInfoList.rowTargetItem, _
                   UnitInfoList.colUpdateItem) = UnitInfoList.UpValue
      ' 更新ステータス = 更新成功
      UnitInfoList.HisStatus = UPDATE_SUCCESS
      Call WriteHistory(UnitIndex)
    
    ' Composite = ADD_IF_NE_2NDの場合
    ' (現在の最新値の2番目の要素が、更新値の2番目の要素と異なる場合、追記する)
    Case COMP_ADD_IF_NE_2ND
      ' 先に更新後の値を辞書より作成する
      vSearchKey = Split(Cells(rowCur, ColList.colSearchKey), ",")
      For i = 0 To UBound(vSearchKey)
        If (ItemDic.Exists(vSearchKey(i)) = False) Then
          ' SearchKeyに指定した、更新対象項目名が辞書に無い場合、その旨を更新履歴に残す
          UnitInfoList.HisStatus = UpdateListSheet & "シートの" & _
                                   UnitInfoList.UpdateItem & "において、" & vbCrLf & _
                                   vSearchKey(i) & "を指定した行より後にComposite指定して下さい。"
          ' UpValueは空にしておく
          UnitInfoList.UpValue = ""
          Call WriteErrorHistory(UnitIndex)
          GoTo UPDATE_BLADE_CHECK_SKIP
        End If

        If (i = UBound(vSearchKey)) Then
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i))
        Else
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i)) & ","
        End If
      Next i

      ' 2番目の要素を取得
      str2ndElement = ItemDic.Item(vSearchKey(1))

      Debug.Print "(UpdateBladeInfo):CompositeVal = " & CompositeVal & _
                  " str2ndElement = " & str2ndElement

      ' 更新前の値が無い("")の場合、Splitで空の配列が返るので分ける
      If (UnitInfoList.PreValue <> "") Then
        ' 更新前の値を改行(vbLF)区切りで取り出す
        vPostscriptVal = Split(UnitInfoList.PreValue, vbLf)

        ' Excelの最大LF数に達しているかをチェックする
        If (UBound(vPostscriptVal) >= MAX_LF) Then
          UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                                   UnitInfoList.UpdateItem & vbCrLf & _
                                   "の1セルあたりの改行(LF)数が多すぎるため、" & _
                                   vbCrLf & _
                                   "更新しませんでした。見直して下さい。"
          ' UpValueは空にしておく
          UnitInfoList.UpValue = ""
          Call WriteErrorHistory(UnitIndex)
          GoTo UPDATE_BLADE_CHECK_SKIP
        End If

        Debug.Print "(UpdateBladeInfo):vPostscriptVal(0) = " & vPostscriptVal(0)

        ' １番最初の値が最新情報とみなし、カンマ区切りの2番目の要素と、同じく
        ' 追記するカンマ区切りの2番目の要素が異なる場合のみ更新する

        ' 1番最初の要素がNull文字(改行のみ)だった場合、改行を付与せずに
        ' 無条件で更新する
        If (vPostscriptVal(0) = "") Then
          UnitInfoList.UpValue = CompositeVal & UnitInfoList.PreValue
        Else
          vElement = Split(vPostscriptVal(0), ",")
          ' カンマ区切りで無い場合、無条件で更新する
          If (LBound(vElement) = UBound(vElement)) Then
            UnitInfoList.UpValue = CompositeVal & vbLf & UnitInfoList.PreValue
          ' カンマ区切りの場合
          Else
            If (vElement(1) <> str2ndElement) Then
              UnitInfoList.UpValue = CompositeVal & vbLf & UnitInfoList.PreValue
            Else
              GoTo UPDATE_BLADE_CHECK_SKIP
            End If
          End If
        End If
      ' 更新前の値が無い("")ため、新規の値で直接更新する
      Else
        UnitInfoList.UpValue = CompositeVal
      End If

      Debug.Print "(UpdateBladeInfo):UnitInfoList.UpValue = " & UnitInfoList.UpValue
      Unitws.Cells(UnitInfoList.rowTargetItem, _
                   UnitInfoList.colUpdateItem) = UnitInfoList.UpValue
      ' 更新ステータス = 更新成功
      UnitInfoList.HisStatus = UPDATE_SUCCESS
      Call WriteHistory(UnitIndex)
    End Select

UPDATE_BLADE_CHECK_SKIP:
    rowCur = rowCur + 1
  Loop

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate

End Function

' DeviceType = C-BLADEの場合の物品Update用Function
' [引数]
'   rowNum As Long           ：「Update_List」シート上の現在行
'   UnitIndex As Long        ：検索で見つかったUnitのindex番号
'   strUnitName As String    ：「unitName (at Log)」に記載のUnit名
'   strTempSheet As String   ：作業用シート名
'
' [戻り値]
'   無し
Public Function UpdateCBladeInfo(ByVal rowNum As Long, _
                                 ByVal UnitIndex As Long, _
                                 ByVal strUnitName As String, _
                                 ByVal strTempSheet As String)

  Call UpdateBasicInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

End Function

' DeviceType = PIUの場合の物品Update用Function
' [引数]
'   rowNum As Long           ：「Update_List」シート上の現在行
'   UnitIndex As Long        ：検索で見つかったUnitのindex番号
'   strUnitName As String    ：「unitName (at Log)」に記載のUnit名
'   strTempSheet As String   ：作業用シート名
'
' [戻り値]
'   無し
Public Function UpdatePiuInfo(ByVal rowNum As Long, _
                              ByVal UnitIndex As Long, _
                              ByVal strUnitName As String, _
                              ByVal strTempSheet As String)
  Dim i As Long
  Dim rowCur As Long
  Dim Unitws As Worksheet
  Dim strComposite As String
  Dim ItemDic As Object
  Dim vPostscriptVal As Variant
  Dim vSearchKey As Variant
  Dim vElement As Variant
  Dim CompositeVal As String
  Dim str2ndElement As String

  Debug.Print "(UpdatePiuInfo):rowNum = " & rowNum & _
              " UnitIndex = " & UnitIndex & _
              " strUnitName = " & strUnitName & _
              " strTempSheet = " & strTempSheet

  rowCur = rowNum
  Set Unitws = Worksheets(UnitInfoList.SheetName)
  Set ItemDic = CreateObject("Scripting.Dictionary")

  ' 「unitName (at Log)」の記載が、現在処理中のUnitNameと異なる行に
  ' なるまで繰り返す
  Do Until ((Cells(rowCur, ColList.colUnitName) <> "") And _
            (Cells(rowCur, ColList.colUnitName) <> strUnitName)) Or _
           (rowCur > rowLastUpdateList)
    UnitInfoList.UpdateItem = Cells(rowCur, ColList.colUpdateItem)
    UnitInfoList.SearchKey = Cells(rowCur, ColList.colSearchKey)
    UnitInfoList.SearchKeyOp = Cells(rowCur, ColList.colSearchKeyOption)
    strComposite = Cells(rowCur, ColList.colComposite)
 
    Debug.Print "(UpdatePiuInfo):UpdateItem = " & UnitInfoList.UpdateItem & _
                " SearchKey = " & UnitInfoList.SearchKey & _
                " SearchKeyOp = " & UnitInfoList.SearchKeyOp

    ' 更新対象項目の列を特定
    If (FindHeadColFromList(UnitInfoList.SheetName, _
                            UnitInfoList.UpdateItem, _
                            UnitInfoList.colUpdateItem) = False) Then
      ' 特定できない場合、その旨を更新履歴に残す
      UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                               UnitInfoList.UpdateItem & "の見出し列がありません"
      ' PreValue, UpValueは空にしておく
      UnitInfoList.PreValue = ""
      UnitInfoList.UpValue = ""
      Call WriteErrorHistory(UnitIndex)
      GoTo UPDATE_PIU_CHECK_SKIP
    End If

    ' 更新前の値を保存
    UnitInfoList.PreValue = Unitws.Cells(UnitInfoList.rowTargetItem, _
                                         UnitInfoList.colUpdateItem)
    Debug.Print "(UpdatePiuInfo):UnitInfoList.PreValue = " & UnitInfoList.PreValue & _
                " strComposite = " & strComposite

    ' UpValueは空にしておく
    UnitInfoList.UpValue = ""

    ' Composite = OFFの場合(ログから検索キーを基に値を取得)
    Select Case (strComposite)
    Case COMP_OFF
      ' 更新後の値をログから取得し、UnitInfoList.UpValueに保存
      If (FindTargetPiuFromLog(UnitIndex, _
                               UnitInfoList.SearchKey, _
                               UnitInfoList.SearchKeyOp, _
                               strTempSheet) = False) Then
        ' ログから指定値の検索に失敗した場合、その旨を更新履歴に残す
        UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                                 UnitInfoList.UpdateItem & "がログ中にありません"
        Call WriteErrorHistory(UnitIndex)
        GoTo UPDATE_PIU_CHECK_SKIP
      End If

      ' 更新後の値を、更新対象項目名とのペアで辞書に登録しておく
      ItemDic.Add UnitInfoList.UpdateItem, UnitInfoList.UpValue

      ' 更新前後の値を比較し、異なる場合のみ管理簿を更新する
      ' (更新前後で同値の場合、管理簿は更新せず、更新履歴にも残らない)
      If (UnitInfoList.PreValue <> UnitInfoList.UpValue) Then
        Unitws.Cells(UnitInfoList.rowTargetItem, _
                     UnitInfoList.colUpdateItem) = UnitInfoList.UpValue
        ' 更新ステータス = 更新成功
        UnitInfoList.HisStatus = UPDATE_SUCCESS
        Call WriteHistory(UnitIndex)
      End If

    ' Composite = ADD_IF_NE_ALLの場合
    ' (現在の最新値が更新値と異なる場合、追記する)
    Case COMP_ADD_IF_NE_ALL
      ' 先に更新後の値を辞書より作成する
      vSearchKey = Split(Cells(rowCur, ColList.colSearchKey), ",")
      For i = 0 To UBound(vSearchKey)
        If (ItemDic.Exists(vSearchKey(i)) = False) Then
          ' SearchKeyに指定した、更新対象項目名が辞書に無い場合、その旨を更新履歴に残す
          UnitInfoList.HisStatus = UpdateListSheet & "シートの" & _
                                   UnitInfoList.UpdateItem & "において、" & vbCrLf & _
                                   vSearchKey(i) & "を指定した行より後にComposite指定して下さい。"
          Call WriteErrorHistory(UnitIndex)
          GoTo UPDATE_PIU_CHECK_SKIP
        End If

        If (i = UBound(vSearchKey)) Then
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i))
        Else
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i)) & ","
        End If
      Next i

      Debug.Print "(UpdatePiuInfo):CompositeVal = " & CompositeVal

      ' 更新前の値が無い("")の場合、Splitで空の配列が返るので分ける
      If (UnitInfoList.PreValue <> "") Then
        ' 更新前の値を改行(vbLF)区切りで取り出す
        vPostscriptVal = Split(UnitInfoList.PreValue, vbLf)

        ' Excelの最大LF数に達しているかをチェックする
        If (UBound(vPostscriptVal) >= MAX_LF) Then
          UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                                   UnitInfoList.UpdateItem & vbCrLf & _
                                   "の1セルあたりの改行(LF)数が多すぎるため、" & _
                                   vbCrLf & _
                                   "更新しませんでした。見直して下さい。"
          Call WriteErrorHistory(UnitIndex)
          GoTo UPDATE_PIU_CHECK_SKIP
        End If

        Debug.Print "(UpdatePiuInfo):vPostscriptVal(0) = " & vPostscriptVal(0)

        ' １番最初の値が最新情報とみなし、追記する値と異なる場合のみ更新する
        If (vPostscriptVal(0) <> CompositeVal) Then
          UnitInfoList.UpValue = CompositeVal & vbLf & UnitInfoList.PreValue
        Else
          GoTo UPDATE_PIU_CHECK_SKIP
        End If
      ' 更新前の値が無い("")ため、新規の値で直接更新する
      Else
        UnitInfoList.UpValue = CompositeVal
      End If

      Debug.Print "(UpdatePiuInfo):UnitInfoList.UpValue = " & UnitInfoList.UpValue
      Unitws.Cells(UnitInfoList.rowTargetItem, _
                   UnitInfoList.colUpdateItem) = UnitInfoList.UpValue
      ' 更新ステータス = 更新成功
      UnitInfoList.HisStatus = UPDATE_SUCCESS
      Call WriteHistory(UnitIndex)

    ' Composite = ADD_IF_NE_2NDの場合
    ' (現在の最新値の2番目の要素が、更新値の2番目の要素と異なる場合、追記する)
    Case COMP_ADD_IF_NE_2ND
      ' 先に更新後の値を辞書より作成する
      vSearchKey = Split(Cells(rowCur, ColList.colSearchKey), ",")
      For i = 0 To UBound(vSearchKey)
        If (ItemDic.Exists(vSearchKey(i)) = False) Then
          ' SearchKeyに指定した、更新対象項目名が辞書に無い場合、その旨を更新履歴に残す
          UnitInfoList.HisStatus = UpdateListSheet & "シートの" & _
                                   UnitInfoList.UpdateItem & "において、" & vbCrLf & _
                                   vSearchKey(i) & "を指定した行より後にComposite指定して下さい。"
          ' UpValueは空にしておく
          UnitInfoList.UpValue = ""
          Call WriteErrorHistory(UnitIndex)
          GoTo UPDATE_PIU_CHECK_SKIP
        End If

        If (i = UBound(vSearchKey)) Then
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i))
        Else
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i)) & ","
        End If
      Next i

      ' 2番目の要素を取得
      str2ndElement = ItemDic.Item(vSearchKey(1))

      Debug.Print "(UpdatePiuInfo):CompositeVal = " & CompositeVal & _
                  " str2ndElement = " & str2ndElement

      ' 更新前の値が無い("")の場合、Splitで空の配列が返るので分ける
      If (UnitInfoList.PreValue <> "") Then
        ' 更新前の値を改行(vbLF)区切りで取り出す
        vPostscriptVal = Split(UnitInfoList.PreValue, vbLf)

        ' Excelの最大LF数に達しているかをチェックする
        If (UBound(vPostscriptVal) >= MAX_LF) Then
          UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                                   UnitInfoList.UpdateItem & vbCrLf & _
                                   "の1セルあたりの改行(LF)数が多すぎるため、" & _
                                   vbCrLf & _
                                   "更新しませんでした。見直して下さい。"
          ' UpValueは空にしておく
          UnitInfoList.UpValue = ""
          Call WriteErrorHistory(UnitIndex)
          GoTo UPDATE_PIU_CHECK_SKIP
        End If

        Debug.Print "(UpdatePiuInfo):vPostscriptVal(0) = " & vPostscriptVal(0)

        ' １番最初の値が最新情報とみなし、カンマ区切りの2番目の要素と、同じく
        ' 追記するカンマ区切りの2番目の要素が異なる場合のみ更新する

        ' 1番最初の要素がNull文字(改行のみ)だった場合、改行を付与せずに
        ' 無条件で更新する
        If (vPostscriptVal(0) = "") Then
          UnitInfoList.UpValue = CompositeVal & UnitInfoList.PreValue
        Else
          vElement = Split(vPostscriptVal(0), ",")
          ' カンマ区切りで無い場合、無条件で更新する
          If (LBound(vElement) = UBound(vElement)) Then
            UnitInfoList.UpValue = CompositeVal & vbLf & UnitInfoList.PreValue
          ' カンマ区切りの場合
          Else
            If (vElement(1) <> str2ndElement) Then
              UnitInfoList.UpValue = CompositeVal & vbLf & UnitInfoList.PreValue
            Else
              GoTo UPDATE_PIU_CHECK_SKIP
            End If
          End If
        End If
      ' 更新前の値が無い("")ため、新規の値で直接更新する
      Else
        UnitInfoList.UpValue = CompositeVal
      End If

      Debug.Print "(UpdatePiuInfo):UnitInfoList.UpValue = " & UnitInfoList.UpValue
      Unitws.Cells(UnitInfoList.rowTargetItem, _
                   UnitInfoList.colUpdateItem) = UnitInfoList.UpValue
      ' 更新ステータス = 更新成功
      UnitInfoList.HisStatus = UPDATE_SUCCESS
      Call WriteHistory(UnitIndex)
    End Select

UPDATE_PIU_CHECK_SKIP:
    rowCur = rowCur + 1
  Loop

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate

End Function

' DeviceType = SFPの場合の物品Update用Function
' [引数]
'   rowNum As Long           ：「Update_List」シート上の現在行
'   UnitIndex As Long        ：検索で見つかったUnitのindex番号
'   strUnitName As String    ：「unitName (at Log)」に記載のUnit名
'   strTempSheet As String   ：作業用シート名
'
' [戻り値]
'   無し
Public Function UpdateSfpInfo(ByVal rowNum As Long, _
                              ByVal UnitIndex As Long, _
                              ByVal strUnitName As String, _
                              ByVal strTempSheet As String)

  Call UpdateBasicInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

End Function

' DeviceType = ACOの場合の物品Update用Function
' [引数]
'   rowNum As Long           ：「Update_List」シート上の現在行
'   UnitIndex As Long        ：検索で見つかったUnitのindex番号
'   strUnitName As String    ：「unitName (at Log)」に記載のUnit名
'   strTempSheet As String   ：作業用シート名
'
' [戻り値]
'   無し
Public Function UpdateAcoInfo(ByVal rowNum As Long, _
                              ByVal UnitIndex As Long, _
                              ByVal strUnitName As String, _
                              ByVal strTempSheet As String)

  Call UpdateBasicInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

End Function

' 管理簿に物品を新規登録するためのFunction
' [引数]
'   strDeviceType As String  ：内部処理用のデバイスタイプ名
'   rowNum As Long           ：「Update_List」シート上の現在行
'   UnitIndex As Long        ：検索で見つかったUnitのindex番号
'   strUnitName As String    ：「unitName (at Log)」に記載のUnit名
'   strTempSheet As String   ：作業用シート名
'
' [戻り値]
'   Boolean：True  (管理簿の物品新規登録に成功)
'          ：False (管理簿の物品新規登録に失敗)
Public Function RegisterNewItem(ByVal strDeviceType As String, _
                                ByVal rowNum As Long, _
                                ByVal UnitIndex As Long, _
                                ByVal strUnitName As String, _
                                ByVal strTempSheet As String) As Boolean
  Dim rowLast As Long
  Dim rngLast As Range

  ' 指定シートへ切替
  Worksheets(UnitInfoList.SheetName).Activate
  ' 指定シートのSerial Number列の最終行を特定する
  rowLast = Cells(Rows.Count, UnitInfoList.colSerialNum).End(xlUp).Row
  Set rngLast = Range(Cells(rowLast, UnitInfoList.colSerialNum).Address)
  ' 最終行 + 最終行セルの行数を新規登録する行数とする
  ' (見出しのみだった場合、最終行のセルは結合セルとなる事を考慮)
  UnitInfoList.rowTargetItem = rowLast + rngLast.MergeArea.Rows.Count
  Debug.Print "(RegisterNewItem):UnitInfoList.rowTargetItem = " & _
              UnitInfoList.rowTargetItem
  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate

  ' DeviceTypeによって処理分け
  Select Case (strDeviceType)
    Case BLADE_DEVICE
      If (UnitInfoList.ShelfConf = AGGREGATOR_MODE) Then
        ' ログ上で該当Unitの情報を取得できるかをチェック
        ' (" ssh -p 6022 fujitsu@127.1.1." + Shelf番号を検索)
        If (CheckDipSSHFromLog(UnitIndex, TEMP_SHEET_NAME) = False) Then
          ' チェックNGの場合、更新履歴のUpdate Statusをエラーにして、
          ' 該当Unitの処理SKIP
          UnitInfoList.HisStatus = "ログ上に、" & strUnitName & _
                                   "の搭載Shelfに対応するDIP接続ログ" & vbCrLf & _
                                   "[" & DIP_SSH & UnitInfoList.ShelfNum(UnitIndex) & "]" & _
                                   vbCrLf & _
                                   "がありませんでした。ログを確認して下さい。"
          Call WriteErrorHistory(UnitIndex)

          RegisterNewItem = False
          Exit Function
        End If
      End If

      Call RegisterBladeInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

    Case C_BLADE_DEVICE
      Call RegisterCBladeInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

    Case PIU_DEVICE
      If (UnitInfoList.ShelfConf = AGGREGATOR_MODE) Then
        ' ログ上で該当Unitの情報を取得できるかをチェック
        ' (" ssh -p 6022 fujitsu@127.1.1." + Shelf番号を検索)
        If (CheckDipSSHFromLog(UnitIndex, TEMP_SHEET_NAME) = False) Then
          ' チェックNGの場合、更新履歴のUpdate Statusをエラーにして、
          ' 該当Unitの処理SKIP
          UnitInfoList.HisStatus = "ログ上に、" & strUnitName & _
                                   "の搭載Shelfに対応するDIP接続ログ" & vbCrLf & _
                                   "[" & DIP_SSH & UnitInfoList.ShelfNum(UnitIndex) & "]" & _
                                   vbCrLf & _
                                   "がありませんでした。ログを確認して下さい。"
          Call WriteErrorHistory(UnitIndex)

          RegisterNewItem = False
          Exit Function
        End If
      End If

      Call RegisterPiuInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

    Case SFP_DEVICE
      Call RegisterSfpInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

    Case ACO_DEVICE
      Call RegisterAcoInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

  End Select

  RegisterNewItem = True

End Function

' 基本の物品新規登録用Function(特殊処理無し)
' [引数]
'   rowNum As Long           ：「Update_List」シート上の現在行
'   UnitIndex As Long        ：検索で見つかったUnitのindex番号
'   strUnitName As String    ：「unitName (at Log)」に記載のUnit名
'   strTempSheet As String   ：作業用シート名
'
' [戻り値]
'   無し
Public Function RegisterBasicInfo(ByVal rowNum As Long, _
                                  ByVal UnitIndex As Long, _
                                  ByVal strUnitName As String, _
                                  ByVal strTempSheet As String)
  Dim i As Long
  Dim rowCur As Long
  Dim Unitws As Worksheet
  Dim strComposite As String
  Dim ItemDic As Object
  Dim vSearchKey As Variant
  Dim CompositeVal As String

  Debug.Print "(RegisterBasicInfo):rowNum = " & rowNum & _
              " UnitIndex = " & UnitIndex & _
              " strUnitName = " & strUnitName & _
              " strTempSheet = " & strTempSheet

  rowCur = rowNum
  Set Unitws = Worksheets(UnitInfoList.SheetName)
  Set ItemDic = CreateObject("Scripting.Dictionary")

  ' 「unitName (at Log)」の記載が、現在処理中のUnitNameと異なる行に
  ' なるまで繰り返す
  Do Until ((Cells(rowCur, ColList.colUnitName) <> "") And _
            (Cells(rowCur, ColList.colUnitName) <> strUnitName)) Or _
           (rowCur > rowLastUpdateList)
    UnitInfoList.UpdateItem = Cells(rowCur, ColList.colUpdateItem)
    UnitInfoList.SearchKey = Cells(rowCur, ColList.colSearchKey)
    UnitInfoList.SearchKeyOp = Cells(rowCur, ColList.colSearchKeyOption)
    strComposite = Cells(rowCur, ColList.colComposite)
 
    Debug.Print "(RegisterBasicInfo):UpdateItem = " & UnitInfoList.UpdateItem & _
                " SearchKey = " & UnitInfoList.SearchKey & _
                " SearchKeyOp = " & UnitInfoList.SearchKeyOp

    ' 指定シートへ切替
    Unitws.Activate

    ' Item Name/Serial Number/Iss.を新規登録
    ' 表示形式を「文字列」に設定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colItemName).Address).NumberFormatLocal = "@"
    ' フォント種別を指定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colItemName).Address).Font.Name = "Meiryo UI"
    Unitws.Cells(UnitInfoList.rowTargetItem, _
                 UnitInfoList.colItemName) = UnitInfoList.ItemName
    ' 表示形式を「文字列」に設定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colSerialNum).Address).NumberFormatLocal = "@"
    ' フォント種別を指定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colSerialNum).Address).Font.Name = "Meiryo UI"
    Unitws.Cells(UnitInfoList.rowTargetItem, _
                 UnitInfoList.colSerialNum) = UnitInfoList.SerialNum(UnitIndex)
    ' 表示形式を「文字列」に設定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colIss).Address).NumberFormatLocal = "@"
    ' フォント種別を指定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colIss).Address).Font.Name = "Meiryo UI"
    Unitws.Cells(UnitInfoList.rowTargetItem, _
                 UnitInfoList.colIss) = UnitInfoList.IssueNum(UnitIndex)

    ' 元のシートに切替
    Worksheets(UpdateListSheet).Activate

    ' 更新対象項目の列を特定
    If (FindHeadColFromList(UnitInfoList.SheetName, _
                            UnitInfoList.UpdateItem, _
                            UnitInfoList.colUpdateItem) = False) Then
      ' 特定できない場合、その旨を更新履歴に残す
      UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                               UnitInfoList.UpdateItem & "の見出し列がありません"
      ' PreValue, UpValueは空にしておく
      UnitInfoList.PreValue = ""
      UnitInfoList.UpValue = ""
      Call WriteErrorHistory(UnitIndex)
      GoTo REG_CHECK_SKIP
    End If

    ' 更新前の値は無し("")とする
    UnitInfoList.PreValue = ""
    Debug.Print "(RegisterBasicInfo):UnitInfoList.PreValue = " & UnitInfoList.PreValue & _
                " strComposite = " & strComposite

    ' UpValueは空にしておく
    UnitInfoList.UpValue = ""

    ' Composite = OFFの場合(ログから検索キーを基に値を取得)
    Select Case (strComposite)
    Case COMP_OFF
      ' 更新後の値をログから取得し、UnitInfoList.UpValueに保存
      If (FindTargetItemFromLog(UnitIndex, _
                                UnitInfoList.SearchKey, _
                                strTempSheet) = False) Then
        ' ログから指定値の検索に失敗した場合、その旨を更新履歴に残す
        UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                                 UnitInfoList.UpdateItem & "がログ中にありません"
        Call WriteErrorHistory(UnitIndex)
        GoTo REG_CHECK_SKIP
      End If

      ' 更新後の値を、更新対象項目名とのペアで辞書に登録しておく
      ItemDic.Add UnitInfoList.UpdateItem, UnitInfoList.UpValue

      ' 更新後の値を無条件で新規登録
      Unitws.Cells(UnitInfoList.rowTargetItem, _
                   UnitInfoList.colUpdateItem) = UnitInfoList.UpValue
      ' 更新ステータス = 新規登録
      UnitInfoList.HisStatus = REGISTER_NEW
      Call WriteHistory(UnitIndex)

    ' Composite = ADD_IF_NE_ALLの場合
    ' (現在の最新値が更新値と異なる場合、追記する)
    ' Composite = ADD_IF_NE_2NDの場合
    ' (現在の最新値の2番目の要素が、更新値の2番目の要素と異なる場合、追記する)
    Case COMP_ADD_IF_NE_ALL, COMP_ADD_IF_NE_2ND
      ' 先に更新後の値を辞書より作成する
      vSearchKey = Split(Cells(rowCur, ColList.colSearchKey), ",")
      For i = 0 To UBound(vSearchKey)
        If (ItemDic.Exists(vSearchKey(i)) = False) Then
          ' SearchKeyに指定した、更新対象項目名が辞書に無い場合、その旨を更新履歴に残す
          UnitInfoList.HisStatus = UpdateListSheet & "シートの" & _
                                   UnitInfoList.UpdateItem & "において、" & vbCrLf & _
                                   vSearchKey(i) & "を指定した行より後にComposite指定して下さい。"
          Call WriteErrorHistory(UnitIndex)
          GoTo REG_CHECK_SKIP
        End If

        If (i = UBound(vSearchKey)) Then
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i))
        Else
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i)) & ","
        End If
      Next i

      Debug.Print "(RegisterBasicInfo):CompositeVal = " & CompositeVal

      ' 更新前の値は無い("")ので、無条件で新規登録
      UnitInfoList.UpValue = CompositeVal

      Debug.Print "(RegisterBasicInfo):UnitInfoList.UpValue = " & UnitInfoList.UpValue
      Unitws.Cells(UnitInfoList.rowTargetItem, _
                   UnitInfoList.colUpdateItem) = UnitInfoList.UpValue
      ' 更新ステータス = 新規登録
      UnitInfoList.HisStatus = REGISTER_NEW
      Call WriteHistory(UnitIndex)

    End Select

REG_CHECK_SKIP:
    rowCur = rowCur + 1
  Loop

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate

End Function

' DeviceType = BLADEの場合の物品新規登録用Function
' [引数]
'   rowNum As Long           ：「Update_List」シート上の現在行
'   UnitIndex As Long        ：検索で見つかったUnitのindex番号
'   strUnitName As String    ：「unitName (at Log)」に記載のUnit名
'   strTempSheet As String   ：作業用シート名
'
' [戻り値]
'   無し
Public Function RegisterBladeInfo(ByVal rowNum As Long, _
                                  ByVal UnitIndex As Long, _
                                  ByVal strUnitName As String, _
                                  ByVal strTempSheet As String)
  Dim i As Long
  Dim rowCur As Long
  Dim Unitws As Worksheet
  Dim strComposite As String
  Dim ItemDic As Object
  Dim vSearchKey As Variant
  Dim CompositeVal As String

  Debug.Print "(RegisterBladeInfo):rowNum = " & rowNum & _
              " UnitIndex = " & UnitIndex & _
              " strUnitName = " & strUnitName & _
              " strTempSheet = " & strTempSheet

  rowCur = rowNum
  Set Unitws = Worksheets(UnitInfoList.SheetName)
  Set ItemDic = CreateObject("Scripting.Dictionary")

  ' 「unitName (at Log)」の記載が、現在処理中のUnitNameと異なる行に
  ' なるまで繰り返す
  Do Until ((Cells(rowCur, ColList.colUnitName) <> "") And _
            (Cells(rowCur, ColList.colUnitName) <> strUnitName)) Or _
           (rowCur > rowLastUpdateList)
    UnitInfoList.UpdateItem = Cells(rowCur, ColList.colUpdateItem)
    UnitInfoList.SearchKey = Cells(rowCur, ColList.colSearchKey)
    UnitInfoList.SearchKeyOp = Cells(rowCur, ColList.colSearchKeyOption)
    strComposite = Cells(rowCur, ColList.colComposite)
 
    Debug.Print "(RegisterBladeInfo):UpdateItem = " & UnitInfoList.UpdateItem & _
                " SearchKey = " & UnitInfoList.SearchKey & _
                " SearchKeyOp = " & UnitInfoList.SearchKeyOp

    ' 指定シートへ切替
    Unitws.Activate

    ' Item Name/Serial Number/Iss.を新規登録
    ' 表示形式を「文字列」に設定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colItemName).Address).NumberFormatLocal = "@"
    ' フォント種別を指定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colItemName).Address).Font.Name = "Meiryo UI"
    Unitws.Cells(UnitInfoList.rowTargetItem, _
                 UnitInfoList.colItemName) = UnitInfoList.ItemName
    ' 表示形式を「文字列」に設定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colSerialNum).Address).NumberFormatLocal = "@"
    ' フォント種別を指定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colSerialNum).Address).Font.Name = "Meiryo UI"
    Unitws.Cells(UnitInfoList.rowTargetItem, _
                 UnitInfoList.colSerialNum) = UnitInfoList.SerialNum(UnitIndex)
    ' 表示形式を「文字列」に設定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colIss).Address).NumberFormatLocal = "@"
    ' フォント種別を指定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colIss).Address).Font.Name = "Meiryo UI"
    Unitws.Cells(UnitInfoList.rowTargetItem, _
                 UnitInfoList.colIss) = UnitInfoList.IssueNum(UnitIndex)

    ' 元のシートに切替
    Worksheets(UpdateListSheet).Activate

    ' 更新対象項目の列を特定
    If (FindHeadColFromList(UnitInfoList.SheetName, _
                            UnitInfoList.UpdateItem, _
                            UnitInfoList.colUpdateItem) = False) Then
      ' 特定できない場合、その旨を更新履歴に残す
      UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                               UnitInfoList.UpdateItem & "の見出し列がありません"
      ' PreValue, UpValueは空にしておく
      UnitInfoList.PreValue = ""
      UnitInfoList.UpValue = ""
      Call WriteErrorHistory(UnitIndex)
      GoTo REG_BLADE_CHECK_SKIP
    End If

    ' 更新前の値は無し("")とする
    UnitInfoList.PreValue = ""
    Debug.Print "(RegisterBladeInfo):UnitInfoList.PreValue = " & UnitInfoList.PreValue & _
                " strComposite = " & strComposite

    ' UpValueは空にしておく
    UnitInfoList.UpValue = ""

    ' Composite = OFFの場合(ログから検索キーを基に値を取得)
    Select Case (strComposite)
    Case COMP_OFF
      ' 更新後の値をログから取得し、UnitInfoList.UpValueに保存
      If (FindTargetBladeFromLog(UnitIndex, _
                                 UnitInfoList.SearchKey, _
                                 strTempSheet) = False) Then
        ' ログから指定値の検索に失敗した場合、その旨を更新履歴に残す
        UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                                 UnitInfoList.UpdateItem & "がログ中にありません"
        Call WriteErrorHistory(UnitIndex)
        GoTo REG_BLADE_CHECK_SKIP
      End If

      ' 更新後の値を、更新対象項目名とのペアで辞書に登録しておく
      ItemDic.Add UnitInfoList.UpdateItem, UnitInfoList.UpValue

      ' 更新後の値を無条件で新規登録
      Unitws.Cells(UnitInfoList.rowTargetItem, _
                   UnitInfoList.colUpdateItem) = UnitInfoList.UpValue
      ' 更新ステータス = 新規登録
      UnitInfoList.HisStatus = REGISTER_NEW
      Call WriteHistory(UnitIndex)

    ' Composite = ADD_IF_NE_ALLの場合
    ' (現在の最新値が更新値と異なる場合、追記する)
    ' Composite = ADD_IF_NE_2NDの場合
    ' (現在の最新値の2番目の要素が、更新値の2番目の要素と異なる場合、追記する)
    Case COMP_ADD_IF_NE_ALL, COMP_ADD_IF_NE_2ND
      ' 先に更新後の値を辞書より作成する
      vSearchKey = Split(Cells(rowCur, ColList.colSearchKey), ",")
      For i = 0 To UBound(vSearchKey)
        If (ItemDic.Exists(vSearchKey(i)) = False) Then
          ' SearchKeyに指定した、更新対象項目名が辞書に無い場合、その旨を更新履歴に残す
          UnitInfoList.HisStatus = UpdateListSheet & "シートの" & _
                                   UnitInfoList.UpdateItem & "において、" & vbCrLf & _
                                   vSearchKey(i) & "を指定した行より後にComposite指定して下さい。"
          Call WriteErrorHistory(UnitIndex)
          GoTo REG_BLADE_CHECK_SKIP
        End If

        If (i = UBound(vSearchKey)) Then
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i))
        Else
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i)) & ","
        End If
      Next i

      Debug.Print "(RegisterBladeInfo):CompositeVal = " & CompositeVal

      ' 更新前の値は無い("")ので、無条件で新規登録
      UnitInfoList.UpValue = CompositeVal

      Debug.Print "(RegisterBladeInfo):UnitInfoList.UpValue = " & UnitInfoList.UpValue
      Unitws.Cells(UnitInfoList.rowTargetItem, _
                   UnitInfoList.colUpdateItem) = UnitInfoList.UpValue
      ' 更新ステータス = 新規登録
      UnitInfoList.HisStatus = REGISTER_NEW
      Call WriteHistory(UnitIndex)

    End Select

REG_BLADE_CHECK_SKIP:
    rowCur = rowCur + 1
  Loop

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate

End Function

' DeviceType = C-BLADEの場合の物品新規登録用Function
' [引数]
'   rowNum As Long           ：「Update_List」シート上の現在行
'   UnitIndex As Long        ：検索で見つかったUnitのindex番号
'   strUnitName As String    ：「unitName (at Log)」に記載のUnit名
'   strTempSheet As String   ：作業用シート名
'
' [戻り値]
'   無し
Public Function RegisterCBladeInfo(ByVal rowNum As Long, _
                                   ByVal UnitIndex As Long, _
                                   ByVal strUnitName As String, _
                                   ByVal strTempSheet As String)

  Call RegisterBasicInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

End Function

' DeviceType = PIUの場合の物品新規登録用Function
' [引数]
'   rowNum As Long           ：「Update_List」シート上の現在行
'   UnitIndex As Long        ：検索で見つかったUnitのindex番号
'   strUnitName As String    ：「unitName (at Log)」に記載のUnit名
'   strTempSheet As String   ：作業用シート名
'
' [戻り値]
'   無し
Public Function RegisterPiuInfo(ByVal rowNum As Long, _
                                ByVal UnitIndex As Long, _
                                ByVal strUnitName As String, _
                                ByVal strTempSheet As String)
  Dim i As Long
  Dim rowCur As Long
  Dim Unitws As Worksheet
  Dim strComposite As String
  Dim ItemDic As Object
  Dim vSearchKey As Variant
  Dim CompositeVal As String

  Debug.Print "(RegisterPiuInfo):rowNum = " & rowNum & _
              " UnitIndex = " & UnitIndex & _
              " strUnitName = " & strUnitName & _
              " strTempSheet = " & strTempSheet

  rowCur = rowNum
  Set Unitws = Worksheets(UnitInfoList.SheetName)
  Set ItemDic = CreateObject("Scripting.Dictionary")

  ' 「unitName (at Log)」の記載が、現在処理中のUnitNameと異なる行に
  ' なるまで繰り返す
  Do Until ((Cells(rowCur, ColList.colUnitName) <> "") And _
            (Cells(rowCur, ColList.colUnitName) <> strUnitName)) Or _
           (rowCur > rowLastUpdateList)
    UnitInfoList.UpdateItem = Cells(rowCur, ColList.colUpdateItem)
    UnitInfoList.SearchKey = Cells(rowCur, ColList.colSearchKey)
    UnitInfoList.SearchKeyOp = Cells(rowCur, ColList.colSearchKeyOption)
    strComposite = Cells(rowCur, ColList.colComposite)
 
    Debug.Print "(RegisterPiuInfo):UpdateItem = " & UnitInfoList.UpdateItem & _
                " SearchKey = " & UnitInfoList.SearchKey & _
                " SearchKeyOp = " & UnitInfoList.SearchKeyOp

    ' 指定シートへ切替
    Unitws.Activate

    ' Item Name/Serial Number/Iss.を新規登録
    ' 表示形式を「文字列」に設定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colItemName).Address).NumberFormatLocal = "@"
    ' フォント種別を指定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colItemName).Address).Font.Name = "Meiryo UI"
    Unitws.Cells(UnitInfoList.rowTargetItem, _
                 UnitInfoList.colItemName) = UnitInfoList.ItemName
    ' 表示形式を「文字列」に設定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colSerialNum).Address).NumberFormatLocal = "@"
    ' フォント種別を指定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colSerialNum).Address).Font.Name = "Meiryo UI"
    Unitws.Cells(UnitInfoList.rowTargetItem, _
                 UnitInfoList.colSerialNum) = UnitInfoList.SerialNum(UnitIndex)
    ' 表示形式を「文字列」に設定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colIss).Address).NumberFormatLocal = "@"
    ' フォント種別を指定
    Range(Unitws.Cells(UnitInfoList.rowTargetItem, _
                       UnitInfoList.colIss).Address).Font.Name = "Meiryo UI"
    Unitws.Cells(UnitInfoList.rowTargetItem, _
                 UnitInfoList.colIss) = UnitInfoList.IssueNum(UnitIndex)

    ' 元のシートに切替
    Worksheets(UpdateListSheet).Activate

    ' 更新対象項目の列を特定
    If (FindHeadColFromList(UnitInfoList.SheetName, _
                            UnitInfoList.UpdateItem, _
                            UnitInfoList.colUpdateItem) = False) Then
      ' 特定できない場合、その旨を更新履歴に残す
      UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                               UnitInfoList.UpdateItem & "の見出し列がありません"
      ' PreValue, UpValueは空にしておく
      UnitInfoList.PreValue = ""
      UnitInfoList.UpValue = ""
      Call WriteErrorHistory(UnitIndex)
      GoTo REG_PIU_CHECK_SKIP
    End If

    ' 更新前の値は無し("")とする
    UnitInfoList.PreValue = ""
    Debug.Print "(RegisterPiuInfo):UnitInfoList.PreValue = " & UnitInfoList.PreValue & _
                " strComposite = " & strComposite

    ' UpValueは空にしておく
    UnitInfoList.UpValue = ""

    ' Composite = OFFの場合(ログから検索キーを基に値を取得)
    Select Case (strComposite)
    Case COMP_OFF
      ' 更新後の値をログから取得し、UnitInfoList.UpValueに保存
      If (FindTargetPiuFromLog(UnitIndex, _
                               UnitInfoList.SearchKey, _
                               UnitInfoList.SearchKeyOp, _
                               strTempSheet) = False) Then
        ' ログから指定値の検索に失敗した場合、その旨を更新履歴に残す
        UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & _
                                 UnitInfoList.UpdateItem & "がログ中にありません"
        Call WriteErrorHistory(UnitIndex)
        GoTo REG_PIU_CHECK_SKIP
      End If

      ' 更新後の値を、更新対象項目名とのペアで辞書に登録しておく
      ItemDic.Add UnitInfoList.UpdateItem, UnitInfoList.UpValue

      ' 更新後の値を無条件で新規登録
      Unitws.Cells(UnitInfoList.rowTargetItem, _
                   UnitInfoList.colUpdateItem) = UnitInfoList.UpValue
      ' 更新ステータス = 新規登録
      UnitInfoList.HisStatus = REGISTER_NEW
      Call WriteHistory(UnitIndex)

    ' Composite = ADD_IF_NE_ALLの場合
    ' (現在の最新値が更新値と異なる場合、追記する)
    ' Composite = ADD_IF_NE_2NDの場合
    ' (現在の最新値の2番目の要素が、更新値の2番目の要素と異なる場合、追記する)
    Case COMP_ADD_IF_NE_ALL, COMP_ADD_IF_NE_2ND
      ' 先に更新後の値を辞書より作成する
      vSearchKey = Split(Cells(rowCur, ColList.colSearchKey), ",")
      For i = 0 To UBound(vSearchKey)
        If (ItemDic.Exists(vSearchKey(i)) = False) Then
          ' SearchKeyに指定した、更新対象項目名が辞書に無い場合、その旨を更新履歴に残す
          UnitInfoList.HisStatus = UpdateListSheet & "シートの" & _
                                   UnitInfoList.UpdateItem & "において、" & vbCrLf & _
                                   vSearchKey(i) & "を指定した行より後にComposite指定して下さい。"
          Call WriteErrorHistory(UnitIndex)
          GoTo REG_PIU_CHECK_SKIP
        End If

        If (i = UBound(vSearchKey)) Then
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i))
        Else
          CompositeVal = CompositeVal & ItemDic.Item(vSearchKey(i)) & ","
        End If
      Next i

      Debug.Print "(RegisterPiuInfo):CompositeVal = " & CompositeVal

      ' 更新前の値は無い("")ので、無条件で新規登録
      UnitInfoList.UpValue = CompositeVal

      Debug.Print "(RegisterPiuInfo):UnitInfoList.UpValue = " & UnitInfoList.UpValue
      Unitws.Cells(UnitInfoList.rowTargetItem, _
                   UnitInfoList.colUpdateItem) = UnitInfoList.UpValue
      ' 更新ステータス = 新規登録
      UnitInfoList.HisStatus = REGISTER_NEW
      Call WriteHistory(UnitIndex)

    End Select

REG_PIU_CHECK_SKIP:
    rowCur = rowCur + 1
  Loop

  ' 元のシートに切替
  Worksheets(UpdateListSheet).Activate

End Function

' DeviceType = SFPの場合の物品新規登録用Function
' [引数]
'   rowNum As Long           ：「Update_List」シート上の現在行
'   UnitIndex As Long        ：検索で見つかったUnitのindex番号
'   strUnitName As String    ：「unitName (at Log)」に記載のUnit名
'   strTempSheet As String   ：作業用シート名
'
' [戻り値]
'   無し
Public Function RegisterSfpInfo(ByVal rowNum As Long, _
                                ByVal UnitIndex As Long, _
                                ByVal strUnitName As String, _
                                ByVal strTempSheet As String)

  Call RegisterBasicInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

End Function

' DeviceType = ACOの場合の物品新規登録用Function
' [引数]
'   rowNum As Long           ：「Update_List」シート上の現在行
'   UnitIndex As Long        ：検索で見つかったUnitのindex番号
'   strUnitName As String    ：「unitName (at Log)」に記載のUnit名
'   strTempSheet As String   ：作業用シート名
'
' [戻り値]
'   無し
Public Function RegisterAcoInfo(ByVal rowNum As Long, _
                                ByVal UnitIndex As Long, _
                                ByVal strUnitName As String, _
                                ByVal strTempSheet As String)

  Call RegisterBasicInfo(rowNum, UnitIndex, strUnitName, strTempSheet)

End Function

