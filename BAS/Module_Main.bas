Attribute VB_Name = "Module_Main"
Option Explicit

' **** Update_Listシート上の関連定義 ****
' データ検索開始行
Public Const ROW_START As Long = 8
' DeviceType定義
Public Const BLADE_DEVICE As String = "BLADE"
Public Const C_BLADE_DEVICE As String = "C-BLADE"
Public Const PIU_DEVICE As String = "PIU"
Public Const SFP_DEVICE As String = "SFP"
Public Const ACO_DEVICE As String = "ACO"
' Update_Listシート上で設定する物品判定モード定義(文字列)
Public Const MODE_SN_ONLY As String = "SN_ONLY"
Public Const MODE_SN_NAME As String = "SN_NAME"
Public Const MODE_SN_NAME_ISS As String = "SN_NAME_ISS"
' Update_Listシート上で設定するパラメータ合成モード
Public Const COMP_OFF As String = "OFF"
Public Const COMP_ADD_IF_NE_ALL As String = "ADD_IF_NE_ALL"
Public Const COMP_ADD_IF_NE_2ND As String = "ADD_IF_NE_2ND"

' Update_Listシートの見出し列番号型定義
Type tColList
  colSheetName As Long
  colItemName As Long
  colOpeMode As Long
  colUnitName As Long
  colComposite As Long
  colUpdateItem As Long
  colSearchKey As Long
  colDeviceType As Long
  colSearchKeyOption As Long
End Type
' Update_Listシートの見出し列番号情報
Public ColList As tColList

' Update_Listシート名
Public UpdateListSheet As String
' Update_Listシートの最終行
Public rowLastUpdateList As Long

' **** 各Unit名シート(管理簿)上の関連定義 ****
' 各Unit毎のシートにおけるUnit名の見出し文字列
Public Const UT_NAME_HEAD As String = "Item Name"
' 各Unit毎のシートにおけるSerial Numberの見出し文字列
Public Const UT_SERIAL_HEAD As String = "Serial" & vbLf & "Number"
' 各Unit毎のシートにおけるIssueの見出し文字列
Public Const UT_ISSUE_HEAD As String = "Iss."
' 各Unit毎のシートにおけるVendor Nameの見出し文字列
Public Const UT_VENDOR_HEAD As String = "Vender"
' 各Unit毎のシートにおけるFC Numberの見出し文字列
Public Const UT_FC_HEAD As String = "Part Number"
' 各Unit毎のシートにおけるCLEIの見出し文字列
Public Const UT_CLEI_HEAD As String = "CLEI"
' 各Unit毎のシートにおけるDomの見出し文字列
Public Const UT_DOM_HEAD As String = "Manufacture" & vbLf & "Year-Month"

' **** ログファイル上の関連定義 ****
' Serial Number検索文字列
Public Const LOG_SERIAL_NUM As String = "serialNumber"
' Issue Number検索文字列
Public Const LOG_ISSUE_NUM As String = "IssueNumber"
' Inventory Name検索文字列
Public Const LOG_INVENTORY_NAME As String = "inventoryName"
' shelf検索文字列
Public Const LOG_SHELF As String = "Shelf:"
' slot検索文字列
Public Const LOG_SLOT As String = "slot:"
' subslot検索文字列
Public Const LOG_SUB_SLOT As String = "subslot:"
' CFP2のUnitName
Public Const LOG_CFP2 As String = "CFP2"
' Vendor Name検索文字列
Public Const LOG_VENDOR_NAME As String = "vendorName"
' FC Number検索文字列
Public Const LOG_FC_NUM As String = "fcNumber"
' CLEI検索文字列
Public Const LOG_CLEI As String = "clei"
' Dom検索文字列
Public Const LOG_DOM As String = "dom"

' **** 更新履歴シート上の関連定義 ****
' Update Statusの定型文字列
Public Const UPDATE_SUCCESS As String = "更新成功"
Public Const REGISTER_NEW As String = "新規登録"

' 更新履歴シート名の先頭文字
Public Const HISTORY_SHEET As String = "History_"
' 更新履歴シートの見出し列番号情報
Public Enum eHistoryHead
  UpdateSheet = 1
  ItemName
  SerialNum
  IssueNum
  UpdateItem
  PreValue
  UpdateValue
  UpdateStatus
  LogFileName
End Enum
' 更新履歴シート名
Public HistorySheet As String
' 更新履歴記入シートの書込み行
Public rowCurHistory As Long

' **** 本ツールにおける処理上の定義 ****
' プログレスバー表示用Maxカウンタ
Public ProgressInfoMaxCounter As Long

' LogFileを格納するフォルダ名
Public Const LOG_DIR_NAME As String = "Log"
' LogFileの拡張子
Public Const LOG_FILE_EXT As String = "*.log"
' LogFileをExcelシートへ出力する際のシート名
Public Const TEMP_SHEET_NAME As String = "temp"

' Excel 1セルあたりの最大ラインフィード数
Public Const MAX_LF As Long = 253

' 特殊処理用の検索キー定義
Public Const PIU_UNIQUE_PROCESS_01 As String = "PIU_UNIQUE_PROCESS_01"
Public Const BLADE_UNIQUE_PROCESS_01 As String = "BLADE_UNIQUE_PROCESS_01"
Public Const BLADE_UNIQUE_PROCESS_02 As String = "BLADE_UNIQUE_PROCESS_02"
Public Const CMN_ISSUE_NUMBER As String = "CMN_ISSUE_NUMBER"
Public Const CMN_FC_NUMBER As String = "CMN_FC_NUMBER"
Public Const CMN_CLEI As String = "CMN_CLEI"
Public Const CMN_VENDOR_NAME As String = "CMN_VENDOR_NAME"
Public Const CMN_DOM As String = "CMN_DOM"

' 更新履歴シートを残す世代数
Public Const HISTORY_GEN_MAX As Long = 3

' FindTargetRowFromListの返り値定義
Public Enum eFindTargetRowFromList
  ITEM_FOUND_SUCCESS = 0
  ITEM_NOT_FOUND
  MULTI_ITEM_FOUND
End Enum

' C200, C220のUnitName定義
Public Const UNIT_C200 As String = "BDC2-C200"
Public Const UNIT_C220 As String = "BDC2-C220"

' Aggrigator構成時のDIP切替ログチェック用文字列
Public Const DIP_SSH As String = " ssh -p 6022 fujitsu@127.1.1."
Public Const DIP_END As String = "Connection to 127.1.1."

' U-BOOT(bootVersion)/exp Boot Version(expBootVersion)検索用の検索キー
Public Const FW_INFO As String = "fw-info "
Public Const BOOT_VER As String = "bootVersion"
Public Const EXT_BOOT_VER As String = "expBootVersion"

' FindUnitInfoFromLog用のDirection引数定義
Public Const SEARCH_NEXT As String = "Next"
Public Const SEARCH_PREV As String = "Prev"

' Shelf構成判別結果の定義
Public Enum eShelfConf
  INIT_STATUS = 0
  STANDALONE_MODE
  AGGREGATOR_MODE
End Enum

' 更新対象Unit関連型定義
Type tUnitInfoList
  ' Update_Listシート
  SheetName As String      ' Sheet名
  ItemName As String       ' Item名
  OpeMode As String        ' 物品判別モード
  UnitName As String       ' Unit名
  UpdateItem As String     ' 更新対象項目名
  SearchKey As String      ' 検索キー情報
  SearchKeyOp As String    ' 検索キーオプション情報
  DeviceType As String     ' デバイスタイプ[内部処理用]
  ' 各Unitシート
  colItemName As Long      ' Item Nameの列番号
  colSerialNum As Long     ' Serial Numberの列番号
  colIss As Long           ' Issueの列番号
  colVendorName As Long    ' Vendor Nameの列番号
  colFcNum As Long         ' Part Number(= FC Number)の列番号
  colClei As Long          ' CLEIの列番号
  colDom As Long           ' Domの列番号
  colUpdateItem As Long    ' 更新対象項目名の列番号
  rowTargetItem As Long    ' 更新対象Item行
  ' ログ出力シート
  SKeyValue As String      ' 検索キー情報で取得した値
  SOpKeyValue As String    ' 検索キーオプション情報で取得した値
  FindNum As Long          ' 検索HitしたUnitName個数
  RngFind() As Range       ' 検索Hitしたセル[動的配列]
  RngDipSSH() As Range     ' 検索HitしたDip切替開始を示すセル[動的配列]
  RngDipEnd() As Range     ' 検索HitしたDip切替終了を示すセル[動的配列]
  SerialNum() As String    ' Serial Number[動的配列]
  IssueNum() As String     ' Issue Number[動的配列]
  ShelfNum() As String     ' Shelf Number[動的配列]
  SlotNum() As String      ' Slot Number[動的配列]
  VendorName() As String   ' Vendor Name[動的配列]
  FcNum() As String        ' FC Number[動的配列]
  Clei() As String         ' CLEI[動的配列]
  Dom() As String          ' Dom[動的配列]
  ShelfConf As eShelfConf  ' Shelf構成判別結果
  ' 更新履歴シート
  LogFileName As String    ' 検索元ログファイル名
  PreValue As String       ' 更新前の値
  UpValue As String        ' 更新後の値
  HisStatus As String      ' 更新ステータス
End Type

' 更新対象Unit関連情報
Public UnitInfoList As tUnitInfoList


'*******************************************************************************
' ログ取り込み＆管理簿更新ボタンをクリック
'
' [引数]
' なし
'
' [戻り値]
' なし
'
' [処理概要]
' (1)「Update_List」シートに記載の「Sheet name」が存在するかチェックする
' (2)物品管理ツールと同じフォルダ階層にあるLogフォルダ内のログファイル一覧を取得する
' (3)(2)で取得したログファイル一覧を基に、1個づつログファイルを読込み、Excelシートに
'    出力後、「Update_List」シートに記載の「unitName (at Log)」があるか検索する
' (3-1)「unitName (at Log)」が見つからなかった場合、(3)に戻って次の「unitName (at Log)」
'      を検索する
' (3-2)「unitName (at Log)」が見つかった場合、「Update_list」シートに記載の
'      「Device type」に従った処理を行う
' (4)「Update_List」シートに記載の「Update target item」列の最終行まで(3)の処理を行う
' (5)(2)〜(4)の処理をLogフォルダ内の全ログファイルに対して行う
'*******************************************************************************
Private Sub btnUpdateManagementBook()
  Dim rowCur As Long
  Dim strLogfile() As String
  Dim strLogFileName As String
  Dim strLogFilePath As String
  Dim MinIndex As Long
  Dim MaxIndex As Long
  Dim i As Long, j As Long
  Dim Rtn As eFindTargetRowFromList

  ' セル名より、列番号を取得
  ColList.colSheetName = Range("SHEET_NAME").Column
  ColList.colItemName = Range("ITEM_NAME").Column
  ColList.colOpeMode = Range("OPE_MODE").Column
  ColList.colUnitName = Range("UNIT_NAME").Column
  ColList.colComposite = Range("COMPOSITE").Column
  ColList.colUpdateItem = Range("UPDATE_ITEM").Column
  ColList.colSearchKey = Range("SEARCH_KEY").Column
  ColList.colDeviceType = Range("DEVICE_TYPE").Column
  ColList.colSearchKeyOption = Range("SEARCH_KEY_OPTION").Column

  ' 更新履歴用シート名を定義
  HistorySheet = HISTORY_SHEET & Format(Now, "yyyymmddhhnnss")

  ' 自身のSheet名(Update_List)を取得
  UpdateListSheet = ActiveSheet.Name

  Application.ScreenUpdating = False

  ' 「Update target item」列の最終行を取得
  rowLastUpdateList = Cells(Rows.Count, ColList.colUpdateItem).End(xlUp).Row
  Debug.Print "(btnUpdateManagementBook):rowLastUpdateList = " & rowLastUpdateList

  ' (1)「Update_List」シートに記載の「Sheet name」が存在するかチェックする
  If (CheckSheetExist(rowLastUpdateList) = False) Then
    Application.ScreenUpdating = True
    Exit Sub
  End If

  ' (2)物品管理ツールと同じフォルダ階層にあるLogフォルダ内のログファイル一覧
  '    を取得する
  strLogFilePath = ThisWorkbook.Path & "\" & LOG_DIR_NAME
  Debug.Print "(btnUpdateManagementBook):strLogFilePath = " & strLogFilePath

  ' Logフォルダ内にログファイル(*.log)がひとつも無い場合、強制終了
  If (GetLogFileList(strLogFilePath, strLogfile) = False) Then
    Application.ScreenUpdating = True
    Exit Sub
  End If

  ' (3)(2)で取得したログファイル一覧を基に、1個づつログファイルを読込み、
  '    Excelシートに出力後、「Update_List」シートに記載の「unitName (at Log)」
  '    があるか検索する
  MinIndex = LBound(strLogfile) + 1
  MaxIndex = UBound(strLogfile)
  Debug.Print "(btnUpdateManagementBook):MinIndex = " & MinIndex & _
              " MaxIndex = " & MaxIndex

  ProgressInfoMaxCounter = rowLastUpdateList * MaxIndex
  ProgressInfo.Show vbModeless

  ' 更新履歴用シートを作成
  Call CreateHistorySheet(HistorySheet)

  ' ログファイル毎に繰り返し
  For i = MinIndex To MaxIndex
    strLogFileName = strLogfile(i)
    UnitInfoList.ShelfConf = INIT_STATUS
    UnitInfoList.LogFileName = strLogFileName
    Debug.Print "(btnUpdateManagementBook):strLogFileName = " & strLogFileName

    ' 検索対象ログファイルをシートに出力する
    Call ReadLogFile(strLogFilePath, strLogFileName, TEMP_SHEET_NAME)

    ' Shelf構成(Standalone/Aggregator)をログから判断する
    Call JudgeShelfConf(UnitInfoList.ShelfConf, TEMP_SHEET_NAME)

    ' 「Update_List」シートの最終行まで1行毎に処理
    For rowCur = ROW_START To rowLastUpdateList

      With ProgressInfo
        .ProgressBar1.Value = rowCur + ((i - 1) * rowLastUpdateList)
        .PercentOfProgress.Caption = _
            Int((rowCur + ((i - 1) * rowLastUpdateList)) / ProgressInfoMaxCounter * 100) & _
            "%"
        .Repaint
      End With

      ' (3-2)「unitName (at Log)」記載が有る行の場合
      If (Cells(rowCur, ColList.colUnitName) <> "") Then
        ' 更新対象Unit関連情報(ログファイル/Shelf構成判別結果除く)を初期化
        Call InitUnitInfoList(UnitInfoList)

        UnitInfoList.SheetName = Cells(rowCur, ColList.colSheetName)
        UnitInfoList.ItemName = Cells(rowCur, ColList.colItemName)
        UnitInfoList.OpeMode = Cells(rowCur, ColList.colOpeMode)
        UnitInfoList.UnitName = Cells(rowCur, ColList.colUnitName)
        UnitInfoList.DeviceType = Cells(rowCur, ColList.colDeviceType)
        Debug.Print "(btnUpdateManagementBook):SheetName = " & UnitInfoList.SheetName & _
                    " ItemName = " & UnitInfoList.ItemName & _
                    " OpeMode = " & UnitInfoList.OpeMode & _
                    " UnitName = " & UnitInfoList.UnitName & _
                    " DeviceType = " & UnitInfoList.DeviceType

        ' UnitNameがログ中に存在しない場合、SKIP
        If (FindUnitNameFromLog(UnitInfoList.UnitName, _
                                TEMP_SHEET_NAME) = False) Then
          Debug.Print "(btnUpdateManagementBook):" & UnitInfoList.UnitName & _
                      " is not found."
          GoTo UNIT_NAME_CHECK_SKIP
        End If

        ' UnitNameがログ中に存在する場合、S/Nを検索する
        ' Serial Numberを検索＆保持
        If (FindUnitInfoFromLog(UnitInfoList.UnitName, _
                                LOG_SERIAL_NUM, SEARCH_NEXT, _
                                UnitInfoList.SerialNum, _
                                TEMP_SHEET_NAME) = False) Then
          MsgBox strLogFileName & "中で、" & vbCrLf & _
                 UnitInfoList.UnitName & "に対応する" _
                 & LOG_SERIAL_NUM & "が無い個所があるため処理中断しました。"
          ' プログレスバーの終了処理
          Application.ScreenUpdating = True
          Unload ProgressInfo
          Exit Sub
        End If

        ' Issue Numberを検索＆保持
        If (FindUnitInfoFromLog(UnitInfoList.UnitName, _
                                LOG_ISSUE_NUM, SEARCH_NEXT, _
                                UnitInfoList.IssueNum, _
                                TEMP_SHEET_NAME) = False) Then
          MsgBox strLogFileName & "中で、" & vbCrLf & _
                 UnitInfoList.UnitName & "に対応する" _
                 & LOG_ISSUE_NUM & "が無い個所があるため処理中断しました。"
          ' プログレスバーの終了処理
          Application.ScreenUpdating = True
          Unload ProgressInfo
          Exit Sub
        End If

        ' Vendor Nameを検索＆保持
        If (FindUnitInfoFromLog(UnitInfoList.UnitName, _
                                LOG_VENDOR_NAME, SEARCH_PREV, _
                                UnitInfoList.VendorName, _
                                TEMP_SHEET_NAME) = False) Then
          MsgBox strLogFileName & "中で、" & vbCrLf & _
                 UnitInfoList.UnitName & "に対応する" _
                 & LOG_VENDOR_NAME & "が無い個所があるため処理中断しました。"
          ' プログレスバーの終了処理
          Application.ScreenUpdating = True
          Unload ProgressInfo
          Exit Sub
        End If

        ' FC Numberを検索＆保持
        If (FindUnitInfoFromLog(UnitInfoList.UnitName, _
                                LOG_FC_NUM, SEARCH_NEXT, _
                                UnitInfoList.FcNum, _
                                TEMP_SHEET_NAME) = False) Then
          MsgBox strLogFileName & "中で、" & vbCrLf & _
                 UnitInfoList.UnitName & "に対応する" _
                 & LOG_FC_NUM & "が無い個所があるため処理中断しました。"
          ' プログレスバーの終了処理
          Application.ScreenUpdating = True
          Unload ProgressInfo
          Exit Sub
        End If

        ' CLEIを検索＆保持
        If (FindUnitInfoFromLog(UnitInfoList.UnitName, _
                                LOG_CLEI, SEARCH_NEXT, _
                                UnitInfoList.Clei, _
                                TEMP_SHEET_NAME) = False) Then
          MsgBox strLogFileName & "中で、" & vbCrLf & _
                 UnitInfoList.UnitName & "に対応する" _
                 & LOG_CLEI & "が無い個所があるため処理中断しました。"
          ' プログレスバーの終了処理
          Application.ScreenUpdating = True
          Unload ProgressInfo
          Exit Sub
        End If

        ' Domを検索＆保持
        If (FindUnitInfoFromLog(UnitInfoList.UnitName, _
                                LOG_DOM, SEARCH_NEXT, _
                                UnitInfoList.Dom, _
                                TEMP_SHEET_NAME) = False) Then
          MsgBox strLogFileName & "中で、" & vbCrLf & _
                 UnitInfoList.UnitName & "に対応する" _
                 & LOG_DOM & "が無い個所があるため処理中断しました。"
          ' プログレスバーの終了処理
          Application.ScreenUpdating = True
          Unload ProgressInfo
          Exit Sub
        End If

        ' Shelf & Slot Numberを検索＆保持
        If (FindShelfSlotInfoFromLog(UnitInfoList.UnitName, _
                                     LOG_INVENTORY_NAME, _
                                     UnitInfoList.ShelfNum, _
                                     UnitInfoList.SlotNum, _
                                     TEMP_SHEET_NAME) = False) Then
          MsgBox strLogFileName & "中で、" & vbCrLf & _
                 UnitInfoList.UnitName & _
                 "に対応するSlot Numberが無い個所があるため処理中断しました。"
          ' プログレスバーの終了処理
          Application.ScreenUpdating = True
          Unload ProgressInfo
          Exit Sub
        End If

        ' 指定シートのItem Name列を特定
        If (FindHeadColFromList(UnitInfoList.SheetName, UT_NAME_HEAD, _
                                UnitInfoList.colItemName) = False) Then
          ' Item Name列が特定できない場合、該当Unitの処理SKIP
          GoTo UNIT_NAME_CHECK_SKIP
        End If

        ' 指定シートのSerial Number列を特定
        If (FindHeadColFromList(UnitInfoList.SheetName, UT_SERIAL_HEAD, _
                                UnitInfoList.colSerialNum) = False) Then
          ' Serial Number列が特定できない場合、該当Unitの処理SKIP
          GoTo UNIT_NAME_CHECK_SKIP
        End If

        ' 指定シートのIssue列を特定
        If (FindHeadColFromList(UnitInfoList.SheetName, UT_ISSUE_HEAD, _
                                UnitInfoList.colIss) = False) Then
          ' Issue列が特定できない場合、該当Unitの処理SKIP
          GoTo UNIT_NAME_CHECK_SKIP
        End If

        ' 指定シートのVender列を特定
        If (FindHeadColFromList(UnitInfoList.SheetName, UT_VENDOR_HEAD, _
                                UnitInfoList.colVendorName) = False) Then
          ' Vender列が特定できない場合、colVendorName = 0となり、
          ' 該当項目は更新/新規登録処理されない
        End If

        ' 指定シートのPart Number列を特定
        If (FindHeadColFromList(UnitInfoList.SheetName, UT_FC_HEAD, _
                                UnitInfoList.colFcNum) = False) Then
          ' Part Number列が特定できない場合、colFcNum = 0となり、
          ' 該当項目は更新/新規登録処理されない
        End If

        ' 指定シートのCLEI列を特定
        If (FindHeadColFromList(UnitInfoList.SheetName, UT_CLEI_HEAD, _
                                UnitInfoList.colClei) = False) Then
          ' CLEI列が特定できない場合、colClei = 0となり、
          ' 該当項目は更新/新規登録処理されない
        End If

        ' 指定シートのDom(Manufacture Year-Month)列を特定
        If (FindHeadColFromList(UnitInfoList.SheetName, UT_DOM_HEAD, _
                                UnitInfoList.colDom) = False) Then
          ' Dom列が特定できない場合、colDom = 0となり、
          ' 該当項目は更新/新規登録処理されない
        End If

        ' 検索で見つかった個数分、更新処理を繰り返し
        For j = 1 To UnitInfoList.FindNum

          ' 判定モードに従って、管理簿における対象Unitの更新対象行を特定
          Rtn = FindTargetRowFromList(UnitInfoList.SheetName, _
                                      UnitInfoList.OpeMode, j)
          Select Case (Rtn)
            Case ITEM_FOUND_SUCCESS
              ' 指定Unitの更新対象行が特定できた場合、管理簿を更新する
              If (UpdateExistItem(UnitInfoList.DeviceType, _
                                  rowCur, j, UnitInfoList.UnitName, _
                                  TEMP_SHEET_NAME) = False) Then
                ' 更新処理NGの場合､該当Unitの処理SKIP
                GoTo FIND_NUM_CHECK_SKIP
              End If
            Case ITEM_NOT_FOUND
              ' 指定Unitが見つからなかった場合、管理簿へ新規登録する
              If (RegisterNewItem(UnitInfoList.DeviceType, _
                                  rowCur, j, UnitInfoList.UnitName, _
                                  TEMP_SHEET_NAME) = False) Then
                ' 更新処理NGの場合､該当Unitの処理SKIP
                GoTo FIND_NUM_CHECK_SKIP
              End If
            Case MULTI_ITEM_FOUND
              ' 指定Unitが複数ある場合、その旨を更新履歴に残し、該当Unitの処理SKIP
              UnitInfoList.HisStatus = UnitInfoList.SheetName & "シートにおいて" & vbCrLf & _
                                       " Serial Number = " & UnitInfoList.SerialNum(j) & _
                                       " Issue Number = " & UnitInfoList.IssueNum(i) & _
                                       "が同一の物品が複数あります。見直して下さい。"
              Call WriteErrorHistory(j)
              GoTo FIND_NUM_CHECK_SKIP
          End Select

FIND_NUM_CHECK_SKIP:
        Next j
      End If
      ' (3-1)「unitName (at Log)」が見つからなかった場合、
      '      次の「unitName (at Log)」を検索する

UNIT_NAME_CHECK_SKIP:
    Next rowCur

  Next i

  ' 更新履歴シートの列全体の幅を自動調整
  Call AutoFitHistorySheet(HistorySheet)

  ' ログ出力シートを削除
  Call DeleteLogSheet(TEMP_SHEET_NAME)

  Application.ScreenUpdating = True
  Unload ProgressInfo

  MsgBox "処理完了", vbInformation

End Sub
