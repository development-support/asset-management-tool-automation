Attribute VB_Name = "Module_Import"
Option Explicit

' インポート処理関連型定義
Type tImportType
  ToBookName As String         ' インポート先のBook名
  TargetSheetNum As Long       ' インポート対象のSheet総数
  TargetSheetName() As String  ' インポート対象のSheet名[動的配列]
  FromBookName As String       ' インポート元のBook名
  FromSheetExist() As Boolean  ' インポート元のSheet存在有無
End Type

Public ImportInfo As tImportType  ' インポート処理関連情報


'*******************************************************************************
' 管理簿(旧Ver.)インポートボタンをクリック
'
' [引数]
' なし
'
' [戻り値]
' なし
'
' [処理概要]
' (1)インポート先のBookにおいて、「Update_List」シートの「Sheet name」列に記載の
'    シートが存在するかチェックする。(存在しないシートがあれば即時終了)
' (2)インポートするファイル(*.xlsm)を選択するためのウィンドウを表示させる。
' (3)選択したファイル(*.xlsm)をOpenする。
' (4)「Update_List」シートの「Sheet name」列に記載のシートが(3)のインポート元
'    ファイルに存在するかチェックする。
'    (4-1)存在する場合、インポート先Bookの同名シートを削除して、インポート元の
'         同名シートをコピーする。
'    (4-2)存在しない場合、インポート先Bookの同名シートはそのままにして、
'         インポート元に該当シートが存在しない旨のメッセージを表示して処理継続。
' (5)(4)の処理を(1)で取得したインポート対象のシート全てにおいて行う。
'*******************************************************************************
Private Sub btnImportOldManagementBook()
  Dim rowLast As Long  ' インポート先UpdateListシートの"Sheet name"列の最終行
  Dim strRtnSheet As String
  Dim OpenFileName As String
  Dim i As Long
  Dim index As Long


  ' セル名より、列番号を取得
  ColList.colSheetName = Range("SHEET_NAME").Column
  ColList.colItemName = Range("ITEM_NAME").Column
  ColList.colOpeMode = Range("OPE_MODE").Column
  ColList.colUnitName = Range("UNIT_NAME").Column
  ColList.colComposite = Range("COMPOSITE").Column
  ColList.colUpdateItem = Range("UPDATE_ITEM").Column
  ColList.colSearchKey = Range("SEARCH_KEY").Column
  ColList.colDeviceType = Range("DEVICE_TYPE").Column
  ColList.colSearchKeyOption = Range("SEARCH_KEY_OPTION").Column

  ' ImportInfoを初期化
  Call InitImportInfo(ImportInfo)

  ' 自身のBook名を取得
  ImportInfo.ToBookName = ActiveWorkbook.Name
  strRtnSheet = ActiveSheet.Name

  Application.ScreenUpdating = False

  ' "Sheet name"列の最終行を取得
  rowLast = Cells(Rows.Count, ColList.colSheetName).End(xlUp).Row
  Debug.Print "(btnImportOldManagementBook):rowLast = " & rowLast

  ' (1)インポート先のBookにおいて、「Update_List」シートの「Sheet name」列に記載の
  '    シートが存在するかチェックする。(存在しないシートがあれば即時終了)
  If (CheckImportTargetSheet(rowLast) = False) Then
    Application.ScreenUpdating = True
    Exit Sub
  End If

  ' (2)インポートするファイル(*.xlsm)を選択するためのウィンドウを表示させる。
  OpenFileName = Application.GetOpenFilename(FileFilter:="マクロ,*.xlsm?")
  If OpenFileName <> "False" Then
    ' インポート元のBook名を保存しておく
    ImportInfo.FromBookName = Dir(OpenFileName)
    ' (3)選択したファイル(*.xlsm)をOpenする。
    Workbooks.Open OpenFileName
  Else
    MsgBox "キャンセルされました"
    Exit Sub
  End If

  ' (4)「Update_List」シートの「Sheet name」列に記載のシートが(3)のインポート元
  '    ファイルに存在するかチェックする。
  For i = 1 To ImportInfo.TargetSheetNum
    Workbooks(ImportInfo.FromBookName).Activate

    If (isSheetExist(ImportInfo.TargetSheetName(i)) = True) Then
      ' (4-1)存在する場合、インポート先Bookの同名シートを削除して、
      '      インポート元の同名シートをコピーする。
      Workbooks(ImportInfo.ToBookName).Activate  ' インポート先のBookに切替
      index = Sheets(ImportInfo.TargetSheetName(i)).index
      Debug.Print "(btnImportOldManagementBook):ImportInfo.TargetSheetName(" & i & _
                  ") = " & ImportInfo.TargetSheetName(i) & _
                  " index = " & index

      Application.DisplayAlerts = False
      Sheets(ImportInfo.TargetSheetName(i)).Delete
      Application.DisplayAlerts = True

      Workbooks(ImportInfo.FromBookName).Activate  ' インポート元のBookに切替
      Application.DisplayAlerts = False
      Sheets(ImportInfo.TargetSheetName(i)).Copy _
        Before:=Workbooks(ImportInfo.ToBookName).Sheets(index)
      Application.DisplayAlerts = True
    Else
      ' (4-2)存在しない場合、インポート先Bookの同名シートはそのままにして、
      '      インポート元に該当シートが存在しない旨のメッセージを表示して処理継続。
      MsgBox "インポート元Bookに" & ImportInfo.TargetSheetName(i) & "シート" & vbCrLf & _
             "が無いため、該当シートのコピーをSkipします。"
    End If
  Next i

  Application.DisplayAlerts = False
  ' インポート元のBookを閉じる
  Workbooks(ImportInfo.FromBookName).Close
  Application.DisplayAlerts = True

  ' インポート先のBookをSaveする
  Workbooks(ImportInfo.ToBookName).Save
  Sheets(strRtnSheet).Activate

  MsgBox "インポート完了", vbInformation

End Sub

' 更新対象Unit関連情報(ログファイル名除く)の初期化用Function
' [引数]
'   UnitInfoList As tUnitInfoList：初期化対象の更新対象Unit関連情報
'
' [戻り値]
'   無し
Public Function InitImportInfo(ByRef ImportInfo As tImportType)
  Dim InitInfo As tImportType

  ' 空の同じ定義型の変数をコピーして初期化
  ImportInfo = InitInfo

End Function

' シートチェック＆インポート対象シートカウント用Function
' [引数]
'   rowLast As Long：「Update_List」シートの「Sheet name」列の最終行
'
' [戻り値]
'   Boolean：True  (指定シートは全て存在する)
'          ：False (指定シートの一部が存在しない)
Public Function CheckImportTargetSheet(ByVal rowLast As Long) As Boolean
  Dim rowCur As Long
  Dim strSheetName As String

  ' 最終行まで1行毎に処理
  For rowCur = ROW_START To rowLast
    ' 「Sheet name」の記載がある場合、値を取得して存在をチェック
    If Cells(rowCur, ColList.colSheetName) <> "" Then
      ImportInfo.TargetSheetNum = ImportInfo.TargetSheetNum + 1
      ReDim Preserve ImportInfo.TargetSheetName(ImportInfo.TargetSheetNum)

      strSheetName = Cells(rowCur, ColList.colSheetName)
      ImportInfo.TargetSheetName(ImportInfo.TargetSheetNum) = strSheetName
      Debug.Print "(CheckImportTargetSheet):ImportInfo.TargetSheetName(" & _
                  ImportInfo.TargetSheetNum & ") = " & strSheetName
    Else
      GoTo CHECK_SKIP
    End If

    ' 「Sheet name」に記載のシートが存在しない場合、強制終了
    If isSheetExist(strSheetName) = False Then
      MsgBox strSheetName & "シートがありません", vbExclamation
      CheckImportTargetSheet = False
      Exit Function
    End If

CHECK_SKIP:
  Next rowCur

  Debug.Print "(CheckImportTargetSheet):ImportInfo.TargetSheetNum = " & _
              ImportInfo.TargetSheetNum
  CheckImportTargetSheet = True

End Function
