Attribute VB_Name = "SrcExport"
Option Explicit

'列挙型変数定義
Public Enum ComponentType
    STANDARD_MODULE = 1  '標準モジュール
    CLASS_MODULE = 2     'クラスモジュール
    USER_FORM = 3        'ユーザーフォーム
    EXCEL_OBJECTS = 100  'Excelオブジェクト（ワークブック・シート）
End Enum

'VBAソースコードエクスポート関数
Public Sub ExportVBAFiles()

    '変数定義
    Dim TempComponent As Object
    Dim ExportPath As String

    'エクスポート先ディレクトリの取得
    ExportPath = ThisWorkbook.Path & "\export_" & Format(Now, "YYYYMMDDhhmm")

    'エクスポート先ディレクトリがない場合、ディレクトリ作成
    If Dir(ExportPath) = "" Then
        Call MkDir(ExportPath)
    End If

    'エクスポート先ディレクトリに「BAS」ディレクトリがない場合、ディレクトリ作成
    If Dir(ExportPath & "\BAS") = "" Then
        Call MkDir(ExportPath & "\BAS")
    End If

    'エクスポート先ディレクトリに「CLS」ディレクトリがない場合、ディレクトリ作成
    If Dir(ExportPath & "\CLS") = "" Then
        Call MkDir(ExportPath & "\CLS")
    End If

    'エクスポート先ディレクトリに「FRM」ディレクトリがない場合、ディレクトリ作成
    If Dir(ExportPath & "\FRM") = "" Then
        Call MkDir(ExportPath & "\FRM")
    End If

    'プロジェクトのソースコードをループ
    For Each TempComponent In ThisWorkbook.VBProject.VBComponents
        'タイプ別判定
        Select Case TempComponent.Type
            '標準モジュールの場合、「BAS」ディレクトリへ出力
            Case STANDARD_MODULE
                TempComponent.Export ExportPath & "\BAS\" & TempComponent.Name & ".bas"

            'クラスモジュールの場合、「CLS」ディレクトリへ出力
            Case CLASS_MODULE
                TempComponent.Export ExportPath & "\CLS\" & TempComponent.Name & ".cls"

            'ユーザーフォームの場合、「FRM」ディレクトリへ出力
            Case USER_FORM
                TempComponent.Export ExportPath & "\FRM\" & TempComponent.Name & ".frm"

            'Excelオブジェクト（ワークブック・シート）の場合、「CLS」ディレクトリへ出力
            Case EXCEL_OBJECTS
                TempComponent.Export ExportPath & "\CLS\" & TempComponent.Name & ".cls"
        End Select
    Next

    MsgBox "ソースエクスポート完了。"
End Sub


