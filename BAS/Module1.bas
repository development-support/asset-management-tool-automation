Attribute VB_Name = "Module1"
' All Rights Reserved, Copyright (C) FUJITSU LIMITED 2017

Option Explicit

'実行パラメータエリアの定義名
Private Const setting_define As String = "データ収集パラメータ"
Private Const setting_rule As String = "入力規則設定パラメータ"

'入力シートのヘッダ検索条件
Private Const input_key_str As String = "Item"  '検索文字列
Private Const input_key_col As String = "B"    '検索対象列

Sub データ集計_Click()
    
    Dim dst_book As Workbook    '出力先テーブルのあるブック
    Set dst_book = ThisWorkbook
    
    '-------------------------------
    'データ収集パラメータの取得
    '-------------------------------
    On Error Resume Next
    
    Dim setting_ws As Worksheet 'パラメータの設定シート
    Set setting_ws = ActiveSheet
    
    Dim setting_area As Range
    Set setting_area = setting_ws.Names(setting_define).RefersToRange
    If Err.Number <> 0 Then
        Call MsgBox("パラメータの指定が見つかりません。", vbCritical)
        Err.Clear
        Exit Sub
    End If
    
    Dim dst_table As ListObject '出力先テーブル
    Dim src_list As Range       '入力シート名リストの設定エリア
    Dim src_count As Variant    '入力シート名の総数
    Dim src_column() As Variant
    Dim check_column() As Variant
    
    '入力シート名リストの開始アドレス
    Set src_list = アドレス指定解析(setting_area.Cells(1))
    If src_list Is Nothing Then
        Exit Sub
    End If
    
    With setting_area.Cells(2) '入力シート名の総数
        Set src_list = src_list.Resize(.Value)
        If Err.Number <> 0 Then
            Call MyRangeSelect(.Cells(1))
            Call MsgBox("パラメータの指定に誤りがあります。" & vbCrLf & vbCrLf _
                      & .Offset(, -1).Value & "が不正です。", vbCritical)
            Err.Clear
            Exit Sub
        End If
    End With
    
    With setting_area.Cells(3)  '出力先テーブル名
        '主力先テーブル取得
        Set dst_table = テーブル取得(.Value, dst_book)
        If Err.Number <> 0 Or dst_table Is Nothing Then
            Call MyRangeSelect(.Cells(1))
            Call MsgBox("パラメータの指定に誤りがあります。" & vbCrLf & vbCrLf _
                      & .Offset(, -1).Value & "が不正です。", vbCritical)
            Err.Clear
            Exit Sub
        End If
    End With
    
    'データ終了判定用の列
    If 列番号解析(setting_area.Cells(4), check_column) = False Then
        Exit Sub
    End If
    
    '収集データ列
    If 列番号解析(setting_area.Cells(5), src_column) = False Then
        Exit Sub
    End If
    
    On Error GoTo 0
    
    If MsgBox("データ集計を開始します", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    
    '-------------------------------
    ' 出力エリア初期化
    '-------------------------------
    Dim date_cell As Range
    Set date_cell = setting_area.Cells(1).Offset(-2)
    Call 値設定(date_cell) '実行日時クリア
    
    Call 出力テーブル初期化(dst_table)
    
    '-------------------------------
    ' データ収集実行
    '-------------------------------
    Dim src_book As Workbook    '入力シートのあるブック
    Dim src_ws As Worksheet
    Dim src_row As Range
    Dim src_cell As Range
    Dim target_cell As Range
    
    Dim col_no As Variant
    Dim i As Variant
    
    Dim dst_row As ListRow
    Dim dst_cell As Range
    Dim data_count As Variant
    Dim msg_str As String
    
    Call MyRangeSelect(dst_table.Range.Cells(1, 1))
    
    Set src_book = ThisWorkbook
    data_count = 0
    For Each target_cell In src_list
        Set src_ws = 入力シート取得(target_cell)
        If src_ws Is Nothing Then
            Exit Sub
        End If
        
        If src_book Is src_ws.Parent Then
            '前回と同じブックから入力
        Else
            If src_book.FullName <> ThisWorkbook.FullName Then
                src_book.Close SaveChanges:=False
            End If
            src_book = src_ws.Parent
        End If
        
        Set src_row = 入力データ行取得(src_ws)
        If src_row Is Nothing Then
            Exit Sub
        End If
        
        Do While 有効データか判定(src_row, check_column)
            On Error Resume Next
            
            data_count = data_count + 1
            If data_count > dst_table.ListRows.Count Then
                Set dst_row = dst_table.ListRows.Add
            Else
                Set dst_row = dst_table.ListRows(data_count)
            End If
            
            dst_row.Range.Columns(1).Value = target_cell.Value
            
            If Err.Number <> 0 Then
                msg_str = "データの設定に失敗しました。" & vbCrLf & vbCrLf _
                        & Err.Description & vbCrLf
                msg_str = msg_str & vbCrLf & "設定先: " & dst_table.Name
                
                Call MyRangeSelect(dst_row.Range)
                Call MsgBox(msg_str, vbCritical)
                Err.Clear
                Exit Sub
            End If
                     
            Call ハイパーリンク設定(dst_row.Range.Columns(2), src_row)
            
            i = 2
            For Each col_no In src_column
                i = i + 1
                Set dst_cell = dst_row.Range.Columns(i)
                
                Set src_cell = src_row.Columns(col_no)
                If src_cell.MergeCells Then
                    Set src_cell = src_cell.MergeArea(1, 1)
                End If
                
                dst_cell.NumberFormatLocal = src_cell.NumberFormatLocal
                dst_cell.Value = src_cell.Value
                
                If Err.Number <> 0 Then
                    Exit For
                End If
            Next col_no
            
            If Err.Number <> 0 Then
                msg_str = "データのコピーに失敗しました。" & vbCrLf & vbCrLf _
                        & Err.Description & vbCrLf
                msg_str = msg_str & vbCrLf & "コピー元: " & src_cell.Address(False, False, External:=True)
                msg_str = msg_str & vbCrLf & "コピー先: " & dst_row.Range.Address(False, False, External:=True)
                
                Call MyRangeSelect(src_cell)
                Call MsgBox(msg_str, vbCritical)
                Err.Clear
                Exit Sub
            End If
            
            On Error GoTo 0
            
            Set src_row = src_row.Offset(1)
        Loop
    Next target_cell
    
    '-------------------------------
    ' 後処理
    '-------------------------------
    If src_book.FullName <> ThisWorkbook.FullName Then
        src_book.Close SaveChanges:=False
    End If
    
    If data_count = 0 Then
        Call MsgBox("入力シートに有効なデータがありません", vbCritical)
        Exit Sub
    End If
    
    Call 値設定(date_cell, Now()) '実行日時設定
    
    dst_table.Range.Columns.AutoFit '列幅調整
    
    If ピボットテーブル更新(dst_book, dst_table.Name) = False Then
        Exit Sub
    End If
    
    Call MsgBox("完了" & vbCrLf & vbCrLf _
                & "抽出データ数= " & data_count)
End Sub

Sub 入力規則設定_Click()
    
    '-------------------------------
    'データ収集パラメータの取得
    '-------------------------------
    On Error Resume Next
    
    Dim setting_ws As Worksheet 'パラメータの設定シート
    Set setting_ws = ActiveSheet
    
    Dim setting_area As Range
    Set setting_area = setting_ws.Names(setting_define).RefersToRange
    If Err.Number <> 0 Then
        Call MsgBox("パラメータの指定が見つかりません。", vbCritical)
        Err.Clear
        Exit Sub
    End If
    
    Dim src_list As Range       '入力シート名リストの設定エリア
    Dim src_count As Variant    '入力シート名の総数
    
    '入力シート名リストの開始アドレス
    Set src_list = アドレス指定解析(setting_area.Cells(1))
    If src_list Is Nothing Then
        Exit Sub
    End If
    
    With setting_area.Cells(2) '入力シート名の総数
        Set src_list = src_list.Resize(.Value)
        If Err.Number <> 0 Then
            Call MyRangeSelect(.Cells(1))
            Call MsgBox("パラメータの指定に誤りがあります。" & vbCrLf & vbCrLf _
                      & .Offset(, -1).Value & "が不正です。", vbCritical)
            Err.Clear
            Exit Sub
        End If
    End With
    
    Dim rule_header As Range
    Set rule_header = setting_ws.Names(setting_rule).RefersToRange
    If Err.Number <> 0 Then
        Call MsgBox("パラメータの指定が見つかりません。", vbCritical)
        Err.Clear
        Exit Sub
    End If
    
    On Error GoTo 0
    
    Dim c1 As Range
    Dim c2 As Range
    Dim rule_count As Variant
    
    rule_count = 0
    Set c1 = rule_header.Offset(1)
    Do Until IsSpaceString(c1.Columns(1).Value)
        rule_count = rule_count + 1
        
        If IsSpaceString(c1.Columns(2).Value) Then
            Call MyRangeSelect(c1.Columns(2))
            Call MsgBox("パラメータの指定に誤りがあります。" & vbCrLf & vbCrLf _
                      & "選択リストが未設定です。", vbCritical)
            Exit Sub
        ElseIf c1.Row = setting_ws.Rows.Count Then
            Exit Do
        End If
        Set c1 = c1.Offset(1)
    Loop
    If rule_count = 0 Then
        Call MyRangeSelect(c1)
        Call MsgBox("入力規則のパラメータが未設定です。", vbCritical)
        Exit Sub
    End If
    
    If MsgBox("入力規則の設定を開始します", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    
    '-------------------------------
    ' 入力規則設定を実行
    '-------------------------------
    Dim src_book As Workbook    '入力シートのあるブック
    Dim src_ws As Worksheet
    Dim top_row As Range
    Dim target_cell As Range
    
    Dim i As Variant
    Dim data_count As Variant
    Dim msg_str As String
    
    Set src_book = ThisWorkbook
    data_count = 0
    For Each target_cell In src_list
        Set src_ws = 入力シート取得(target_cell)
        If src_ws Is Nothing Then
            Exit Sub
        End If
        
        If src_book Is src_ws.Parent Then
            '前回と同じブックから入力
        Else
            If src_book.FullName <> ThisWorkbook.FullName Then
                src_book.Close SaveChanges:=False
            End If
            src_book = src_ws.Parent
        End If
        
        Call MySheetActivate(src_ws)
        
        Set top_row = 入力データ行取得(src_ws)
        If top_row Is Nothing Then
            Exit Sub
        End If
        
        '下側に罫線のある最後の行を検出
        data_count = 0
        Set c1 = top_row.Columns(input_key_col)
        Do While c1.Borders(xlEdgeBottom).LineStyle <> xlNone
            data_count = data_count + 1
            If c1.Row = src_ws.Rows.Count Then
                Exit Do
            End If
            Set c1 = c1.Offset(1)
        Loop
        
        If data_count > 0 Then
            '列毎に入力規則を設定
            Set c1 = rule_header
            For i = 1 To rule_count
                Set c1 = c1.Offset(1)
                
                On Error Resume Next
                Set c2 = top_row.Columns(c1.Columns(1).Value).Resize(data_count)
                
                With c2.Validation
                    .Delete
                    .Add Type:=xlValidateList, _
                            Operator:=xlEqual, _
                            Formula1:=c1.Columns(2).Value
                End With
                
                If Err.Number <> 0 Then
                    msg_str = "入力規則の設定に失敗しました。" & vbCrLf & vbCrLf _
                            & Err.Description & vbCrLf
                    msg_str = msg_str & vbCrLf & "設定先: " & c2.Address(External:=True)
                    
                    Call MyRangeSelect(c2)
                    Call MsgBox(msg_str, vbCritical)
                    Err.Clear
                    Exit Sub
                End If
                
                On Error GoTo 0
            Next i
        End If
    Next target_cell
    
    '-------------------------------
    ' 後処理
    '-------------------------------
    If src_book.FullName <> ThisWorkbook.FullName Then
        src_book.Close SaveChanges:=False
    End If
    
    Call MsgBox("完了")
End Sub

Private Function アドレス指定解析( _
        setting_cell As Range) As Range
        
    Dim ret_cell As Range
    
    On Error Resume Next
    Set ret_cell = setting_cell.Worksheet.Range(setting_cell.Value)
    If Err.Number <> 0 Then
        Set ret_cell = Nothing
    End If
    On Error GoTo 0
        
    If ret_cell Is Nothing Then
        Call MyRangeSelect(setting_cell)
        Call MsgBox("パラメータの指定に誤りがあります。" & vbCrLf & vbCrLf _
                  & setting_cell.Offset(, -1).Value & "の指定が不正です。", vbCritical)
    End If
    
    Set アドレス指定解析 = ret_cell
End Function

Private Function 列番号解析( _
        setting_cell As Range, _
        ByRef ret_val() As Variant) As Boolean
        
    列番号解析 = False
    
    On Error Resume Next
    
    Dim tmp_str As String
    tmp_str = Trim(setting_cell.Value) & ","
    tmp_str = Replace(tmp_str, ",", "1,")
    tmp_str = Replace(tmp_str, ":", "1:")
    tmp_str = Left(tmp_str, Len(tmp_str) - 1)
    
    Dim c1 As Range
    Dim i As Variant
    i = 0
    For Each c1 In Range(tmp_str).Cells
        ReDim Preserve ret_val(i)
        ret_val(i) = c1.Column
        i = i + 1
    Next c1
    If Err.Number <> 0 Then
        Call MyRangeSelect(setting_cell)
        Call MsgBox("パラメータの指定に誤りがあります。" & vbCrLf & vbCrLf _
                  & setting_cell.Offset(, -1).Value & "が不正です。", vbCritical)
        Err.Clear
        Exit Function
    End If
    
    列番号解析 = True
End Function

Private Sub 出力テーブル初期化( _
        対象テーブル As ListObject)
        
    'データの一行目以外を削除
    Dim i As Variant
    For i = 対象テーブル.ListRows.Count To 2 Step -1
        対象テーブル.ListRows(i).Delete
    Next i
    
    'データの一行目の値をクリア。数式は残す
    Dim iter_cell As Range
    Dim row_obj As ListRow
    Set row_obj = 対象テーブル.ListRows(1)
    For Each iter_cell In row_obj.Range.Cells
        If Not iter_cell.HasFormula Then
            iter_cell.ClearContents
        End If
    Next iter_cell
End Sub

Private Function ピボットテーブル更新( _
        対象ブック As Workbook, _
        データソース名 As String) As Boolean
        
    ピボットテーブル更新 = False
    
    Dim iter_pvc As PivotCache
    For Each iter_pvc In 対象ブック.PivotCaches
        If iter_pvc.SourceData = データソース名 Then
          Call iter_pvc.Refresh
        End If
    Next iter_pvc
    
    Dim iter_ws As Worksheet
    Dim iter_pt As PivotTable
    Dim err_list As String
    err_list = ""
    For Each iter_ws In 対象ブック.Worksheets
        For Each iter_pt In iter_ws.PivotTables
            If iter_pt.SourceData = データソース名 Then
                If iter_pt.RefreshTable() = False Then
                    err_list = err_list & vbCrLf & iter_pt.Name
                End If
            End If
        Next iter_pt
    Next iter_ws
    
    If err_list <> "" Then
        Call MsgBox("下記のピボットテーブルの更新に失敗しました" & vbCrLf _
                  & "手動で更新してください" & vbCrLf _
                  & err_list, vbCritical)
        Exit Function
    End If
    
    ピボットテーブル更新 = True
End Function

Private Sub 値設定( _
        設定先セル As Range, _
        Optional ByVal 設定先値 As Variant)
    
    If IsMissing(設定先値) Then
        設定先セル.ClearContents
    Else
        設定先セル.Value = 設定先値
    End If
   
End Sub

Private Function 有効データか判定( _
        target_row As Range, _
        check_columns() As Variant) As Boolean
        
    Dim col_no As Variant
    Dim target_cell As Range
    For Each col_no In check_columns
        Set target_cell = target_row.Columns(col_no)
        If target_cell.MergeCells Then
             Set target_cell = target_cell.MergeArea(1, 1)
        End If
        
        If Not IsSpaceString(target_cell.Value) Then
            有効データか判定 = True
            Exit Function
        End If
    Next col_no
    
    有効データか判定 = False
End Function

Private Function テーブル取得( _
        ByVal テーブル名 As String, _
        検索対象 As Workbook) As ListObject
        
    Dim iter_ws As Worksheet
    Dim iter_tb As ListObject
    For Each iter_ws In 検索対象.Worksheets
        For Each iter_tb In iter_ws.ListObjects
            If iter_tb.Name = テーブル名 Then
                Set テーブル取得 = iter_tb
                Exit Function
            End If
        Next iter_tb
    Next iter_ws
              
    Set テーブル取得 = Nothing
End Function

Private Function 入力シート取得( _
        target_cell As Range) As Worksheet
        
    Set 入力シート取得 = Nothing
    
    Dim src_sheet_name As String    '入力シート名
    Dim src_book As Workbook    '入力シートのあるブック
    Dim src_ws As Worksheet
    
    On Error Resume Next
    
    Select Case target_cell.Hyperlinks.Count
    Case 0
        Set src_book = ThisWorkbook
        src_sheet_name = target_cell.Value
        
    Case Is > 1
        Call MyRangeSelect(target_cell)
        Call MsgBox("入力シートの指定に誤りがあります。" & vbCrLf & vbCrLf _
                  & "ハイパーリンク先が特定できません。", vbCritical)
        Err.Clear
        Exit Function
        
    Case Else
        If target_cell.Hyperlinks(1).Address <> "" Then
            Call MyRangeSelect(target_cell)
            Call MsgBox("入力シートの指定に誤りがあります。" & vbCrLf & vbCrLf _
                      & "ドキュメント外へのハイパーリンクが設定されています。", vbCritical)
            Err.Clear
            Exit Function
        Else
            Set src_book = ThisWorkbook
        End If
        
        src_sheet_name = target_cell.Hyperlinks(1).SubAddress
        If src_sheet_name Like "*!*" Then
            src_sheet_name = Left(src_sheet_name, InStrRev(src_sheet_name, "!") - 1)
        End If
        If src_sheet_name Like "'*'" Then
            src_sheet_name = Mid(src_sheet_name, 2, Len(src_sheet_name) - 2)
        End If
    End Select
    
    If src_sheet_name = "" Then
        Call MyRangeSelect(target_cell)
        Call MsgBox("入力シートの指定に誤りがあります。" & vbCrLf & vbCrLf _
                  & "シート名の指定がありません。", vbCritical)
        Err.Clear
        Exit Function
    End If
    
    Set src_ws = src_book.Worksheets(src_sheet_name)
    If Err.Number <> 0 Then
        Call MsgBox("パラメータの指定に誤りがあります。" & vbCrLf & vbCrLf _
                  & src_book.Name & " に" & vbCrLf _
                  & "'" & src_sheet_name & "'シートがありません", vbCritical)
        Err.Clear
        Exit Function
    End If
    
    On Error GoTo 0
    
    Set 入力シート取得 = src_ws
End Function

'収集データ開始行を検出
Private Function 入力データ行取得( _
        src_ws As Worksheet) As Range
    
    Set 入力データ行取得 = Nothing
    
    Dim max_row As Variant
    With src_ws.UsedRange
        max_row = .Row + .Rows.Count - 1
    End With
    
    Dim start_cell As Range
    Set start_cell = src_ws.Cells(1, input_key_col)
    
    Dim c1 As Range
    Set c1 = start_cell
    Do Until c1.Value = input_key_str
        If c1.Row >= max_row Then
            Call MyRangeSelect(start_cell)
            Call MsgBox("入力シートの指定に誤りがあります。" & vbCrLf & vbCrLf _
                      & input_key_col & "列に'" & input_key_str & "' がありません。", vbCritical)
            Exit Function
        End If
        Set c1 = c1.Offset(1)
    Loop
    If c1.Row >= src_ws.Rows.Count Then
        Call MyRangeSelect(c1)
        Call MsgBox("入力シートの指定に誤りがあります。" & vbCrLf & vbCrLf _
                  & input_key_col & "の下にデータ行がありません。", vbCritical)
        Exit Function
    End If
    
    Set 入力データ行取得 = c1.Offset(1).EntireRow
End Function

Private Sub ハイパーリンク設定( _
        設定セル As Range, _
        リンク先セル As Range)
          
    Dim src_ws As Worksheet
    Dim dst_ws As Worksheet
    Dim src_book As Workbook
    Dim dst_book As Workbook

    Set src_ws = リンク先セル.Worksheet
    Set dst_ws = 設定セル.Worksheet
    
    Set src_book = src_ws.Parent
    Set dst_book = dst_ws.Parent
    
    Dim addr_str As String
    Dim subaddr_str As String
    
    addr_str = src_book.FullName
    subaddr_str = "'" & src_ws.Name & "'!" & リンク先セル.Address(False, False)
    
    If src_book Is dst_book Then
        addr_str = "#" & subaddr_str
        subaddr_str = ""
    End If
    
    On Error Resume Next
    設定セル.Worksheet.Hyperlinks.Add Anchor:=設定セル _
            , Address:=addr_str _
            , SubAddress:=subaddr_str _
            , TextToDisplay:=src_ws.Name
    Err.Clear
End Sub

'----------------------------------------------------------------
'
'   汎用ルーチン
'
'----------------------------------------------------------------
Private Sub MyRangeSelect(target_area As Range)
    '注）WorkSheetがActiveでないとCellのSelectが実行エラーになる
    Call MySheetActivate(target_area.Worksheet)
    target_area.Select
End Sub

'注）BookがActiveでないとWorkSheetのActivateが実行エラーになる
Private Sub MySheetActivate(target_ws As Worksheet)
    target_ws.Parent.Activate
    target_ws.Select
End Sub

Private _
Function IsSpaceString(ByVal 値 As Variant) As Boolean
    
    ' 空白文字の除去
    If MySpaceCharsReplace(値, "") = "" Then
        IsSpaceString = True
    Else
        IsSpaceString = False
    End If

End Function

Private _
Function MySpaceCharsReplace( _
        ByVal 変換対象 As Variant, _
        ByVal 置換文字 As String) As String
    
    Dim strTmp As String
    Dim i As Long
        
    strTmp = MyStringValue(変換対象)
    
    ' 空白文字を半角空白に置換
    strTmp = Replace(strTmp, "　", " ") '全角空白
    strTmp = Replace(strTmp, vbTab, " ")
    strTmp = Replace(strTmp, vbLf, " ")
    strTmp = Replace(strTmp, vbCr, " ")
    strTmp = Replace(strTmp, vbBack, " ")
    
    '連続する空白を１文字に変換
    Do
        i = Len(strTmp)
        strTmp = Replace(strTmp, "  ", " ")
    Loop While Len(strTmp) <> i
    
    MySpaceCharsReplace = Replace(strTmp, " ", 置換文字)
    
End Function

Private _
Function MyStringValue( _
        varTarget As Variant) As String
    
    If IsEmpty(varTarget) Then
        MyStringValue = ""
    ElseIf IsNull(varTarget) Then
        MyStringValue = ""
    Else
        MyStringValue = CStr(varTarget)
    End If
End Function

